package com.getjavajob.training.algo08.mamaeva.lesson05;

import com.getjavajob.training.algo08.mamaeva.lesson04.SinglyLinkedList;

public class LinkedListStack<E> implements Stack<E> {
    private SinglyLinkedList<E> list = new SinglyLinkedList<>();

    @Override
    public void push(E e) {
        list.addHead(e);
    }

    @Override
    public E pop() {
        return list.removeHead();
    }

    public int size() {
        return list.size();
    }
}
