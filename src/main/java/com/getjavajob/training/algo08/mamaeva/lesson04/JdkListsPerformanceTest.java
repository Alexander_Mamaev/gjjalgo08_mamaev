package com.getjavajob.training.algo08.mamaeva.lesson04;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class JdkListsPerformanceTest {
    private static final String GREEN = "\u001B[32m";
    private static final String DEFAULT = "\u001B[0m";

    private static StopWatch time = new StopWatch();
    private static List<Object> arrayList = new ArrayList<>();
    private static List<Object> linkedList = new LinkedList<>();
    private static HashMap<String, Long> result = new HashMap<>();

    public static void main(String[] args) {
        arrayListAddRemoveBeginTest(200_000_000);
        linkedListAddRemoveBeginTest(200_000_000);
        arrayListAddRemoveMiddleTest(200_000);
        linkedListAddRemoveMiddleTest(200_000);
        arrayListAddRemoveEndTest(200_000_000);
        linkedListAddRemoveEndTest(200_000_000);
        System.out.println(GREEN + result.toString() + DEFAULT);
    }

    private static void linkedListAddRemoveEndTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        linkedList = new LinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestLLAddRemoveEnd(size);
            totalRemoveTime += timeTestLLRemoveEnd(linkedList.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average linkedList.add(toEnd) time", totalAddTime / iter);
        result.put("Average linkedList.removeHead(fromEnd) time", totalRemoveTime / iter);
    }

    private static void arrayListAddRemoveEndTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        arrayList = new ArrayList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestALAddEnd(size);
            totalRemoveTime += timeTestALRemovedEnd(arrayList.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average arrayList.add(toEnd) time", totalAddTime / iter);
        result.put("Average arrayList.removeHead(fromEnd) time", totalRemoveTime / iter);
    }

    private static void linkedListAddRemoveMiddleTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        linkedList = new LinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestLLAddMiddle(size);
            totalRemoveTime += timeTestLLRemovedMiddle(linkedList.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average linkedList.add(toMiddle) time", totalAddTime / iter);
        result.put("Average linkedList.removeHead(fromMiddle) time", totalRemoveTime / iter);
    }

    private static void arrayListAddRemoveMiddleTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        arrayList = new ArrayList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestALAddMiddle(size);
            totalRemoveTime += timeTestALRemoveMiddle(arrayList.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average arrayList.add(toMiddle) time", totalAddTime / iter);
        result.put("Average arrayList.removeHead(fromMiddle) time", totalRemoveTime / iter);
    }

    private static void arrayListAddRemoveBeginTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        arrayList = new ArrayList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestALAddBegin(size);
            totalRemoveTime += timeTestALRemovedBegin(arrayList.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average arrayList.add(toBegin) time", totalAddTime / iter);
        result.put("Average arrayList.removeHead(fromBegin) time", totalRemoveTime / iter);
    }

    private static void linkedListAddRemoveBeginTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        linkedList = new LinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestLLAddBegin(size);
            totalRemoveTime += timeTestLLRemovedBegin(linkedList.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average linkedList.add(toBegin) time", totalAddTime / iter);
        result.put("Average linkedList.removeHead(fromBegin) time", totalRemoveTime / iter);
    }

    private static long timeTestALAddBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            arrayList.add("");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("arrayList.add(toBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestALRemovedBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            arrayList.remove(0);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("arrayList.removed(fromBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLAddBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            linkedList.add("");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.add(toBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLRemovedBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            linkedList.remove(0);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.removeHead(fromBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestALAddMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            arrayList.add(arrayList.size() >> 1, "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("arrayList.add(toMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestALRemoveMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            arrayList.remove(arrayList.size() >> 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("arrayList.removeHead(fromMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLAddMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            linkedList.add(linkedList.size() >> 1, "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.add(toMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLRemovedMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            linkedList.remove(linkedList.size() >> 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.removeHead(fromMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestALAddEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            arrayList.add(arrayList.size(), "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("arrayList.add(toEnd): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestALRemovedEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            arrayList.remove(arrayList.size() - 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("arrayList.removeHead(fromEnd): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLAddRemoveEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            linkedList.add(linkedList.size(), "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.add(toEnd): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLRemoveEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            linkedList.remove(linkedList.size() - 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.removeHead(fromEnd): " + totalTime + " ms");
        return totalTime;
    }
}
