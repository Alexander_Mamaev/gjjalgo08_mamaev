package com.getjavajob.training.algo08.mamaeva.lesson06;

import java.util.HashMap;
import java.util.Map;

public class Task2Matrix<V> implements Matrix<V> {
    private Map<Key, V> map = new HashMap<>();

    @Override
    public V get(int i, int j) {
        return map.get(new Key(i, j));
    }

    @Override
    public void set(int i, int j, V value) {
        map.put(new Key(i, j), value);
    }

    public int size() {
        return map.size();
    }

    @Override
    public String toString() {
        return map.toString();
    }
}

class Key {
    private long key;

    public Key(int i, int j) {
        long iL = (long) i;
        long jL = (long) j;
        iL <<= 32;
        key = iL | jL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Key key1 = (Key) o;
        return key == key1.key;
    }

    @Override
    public int hashCode() {
        return (int) (key ^ (key >>> 32));
    }

    @Override
    public String toString() {
        long i = key >> 32;
        long j = (key << 32) >> 32;
        return "Key(" + i + ", " + j + ")";
    }
}
