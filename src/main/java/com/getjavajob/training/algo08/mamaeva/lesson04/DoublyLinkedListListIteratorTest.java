package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.*;

public class DoublyLinkedListListIteratorTest {
    private static List<Object> list = new LinkedList<>();
    private static DoublyLinkedList<Object> dLL = new DoublyLinkedList<>();
    private static int countNewObject;
    private static DoublyLinkedList<Object>.ListIteratorImpl dLLListIterator;
    private static ListIterator<Object> originalListIterator;

    public static void main(String[] args) {
        fillArrays();
        dLLListIterator = dLL.listIterator();
        originalListIterator = list.listIterator();

        cursorEnd();
        testAdd("Test add(toEnd)...");
        testAdd("Test add(toEnd)...");
        testAdd("Test add(toEnd)...");
        stepPrevious();
        testRemove("Test removeHead(fromEnd)...");
        stepPrevious();
        testSet("test set(toEnd)...");
        testSet("test set(toEnd)...");
        cursorStart();
        testSet("Test set(toStart)...");
        testAdd("Test add(toStart)...");
        stepPrevious();
        testRemove("Test removeHead(fromStart)...");
        testAdd("Test add(toStart)...");

        for (int i = 0; i < 7; i++) {
            stepNext();
        }
        testRemove("Test removeHead(fromMiddle)...");
        stepNext();
        testAdd("Test add(toMiddle)...");
        stepNext();
        testSet("Test set(to Midlle)...");

        for (int i = 0; i < 5; i++) {
            stepPrevious();
        }
        testRemove("Test removeHead(fromMiddle)...");
        stepNext();
        testAdd("Test add(toMiddle)...");
        stepNext();
        testSet("Test set(to Midlle)...");
        stepNext();
        testAddCME("Test ConcurrentModificationException from add()...");
        testRemoveCME("Test ConcurrentModificationException from removeHead()...");
        testSetCME("Test ConcurrentModificationException from set()...");
        testNextCME("Test ConcurrentModificationException from next()...");
        testPreviousCME("Test ConcurrentModificationException from previous()...");

        printTestResult();
    }

    private static void testPreviousCME(String msg) {
        dLLListIterator = dLL.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dLL.add(getNewObject());
            dLL.remove(dLL.size() - 1);
            dLLListIterator.previous();
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testNextCME(String msg) {
        dLLListIterator = dLL.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dLL.add(getNewObject());
            dLL.remove(dLL.size() - 1);
            dLLListIterator.next();
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testSetCME(String msg) {
        dLLListIterator = dLL.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dLL.add(getNewObject());
            dLL.remove(dLL.size() - 1);
            dLLListIterator.set(getNewObject());
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testRemoveCME(String msg) {
        dLLListIterator = dLL.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dLL.add(getNewObject());
            dLL.remove(dLL.size() - 1);
            dLLListIterator.remove();
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testAddCME(String msg) {
        try {
            dLL.add(getNewObject());
            dLL.remove(dLL.size() - 1);
            dLLListIterator.add(getNewObject());
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testAdd(String msg) {
        Object newObject = getNewObject();
        originalListIterator.add(newObject);
        dLLListIterator.add(newObject);
        assertEquals(msg, list.toArray(), dLL.toArray());
    }

    private static void testSet(String msg) {
        Object o = getNewObject();
        dLLListIterator.set(o);
        originalListIterator.set(o);
        assertEquals(msg, list.toArray(), dLL.toArray());
    }

    private static void testRemove(String msg) {
        dLLListIterator.remove();
        originalListIterator.remove();
        assertEquals(msg, list.toArray(), dLL.toArray());
    }

    //used for debugging
    private static void printArrays() {
        System.out.println("DA.nextIndex:" + dLLListIterator.nextIndex());
        System.out.print(Arrays.toString(dLL.toArray()) + System.lineSeparator());
        System.out.println("list.nextIndex:" + originalListIterator.nextIndex());
        System.out.print(Arrays.toString(list.toArray()) + System.lineSeparator());
    }

    private static void cursorEnd() {
        while (dLLListIterator.hasNext()) {
            stepNext();
        }
    }

    private static void cursorStart() {
        while (originalListIterator.hasPrevious()) {
            stepPrevious();
        }
    }

    private static void stepNext() {
        dLLListIterator.next();
        originalListIterator.next();
    }

    private static void stepPrevious() {
        dLLListIterator.previous();
        originalListIterator.previous();
    }

    private static void fillArrays() {
        for (int i = 0; i < 15; i++) {
            String string = "Object" + i;
            dLL.add(string);
            list.add(string);
        }
    }

    private static Object getNewObject() {
        return "newObject" + countNewObject++;
    }
}
