package com.getjavajob.training.algo08.mamaeva.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class BalanceableTreeTest {
    public static void main(String[] args) {
        rotationTests();

        reduceSubtreeHeightCase1Test();
        reduceSubtreeHeightCase2Test();
        reduceSubtreeHeightCase3Test();
        reduceSubtreeHeightCase4Test();

        printTestResult();
    }

    private static void reduceSubtreeHeightCase4Test() {
        BalanceableTree<Integer> bt = new BalanceableTree<>();
        Node<Integer> n1 = bt.add(1);
        Node<Integer> n3 = bt.add(3);
        Node<Integer> n2 = bt.add(2);

        assertEquals("reduceSubtreeHeightCase4Test... root of subTree...", n2, bt.reduceSubtreeHeight(n2));
        String expected = "(2(1, 3))";
        assertEquals("reduceSubtreeHeightCase4Test...", expected, bt.toString());
    }

    private static void reduceSubtreeHeightCase3Test() {
        BalanceableTree<Integer> bt = new BalanceableTree<>();
        Node<Integer> n3 = bt.add(3);
        Node<Integer> n1 = bt.add(1);
        Node<Integer> n2 = bt.add(2);

        assertEquals("reduceSubtreeHeightCase3Test... root of subTree...", n2, bt.reduceSubtreeHeight(n2));
        String expected = "(2(1, 3))";
        assertEquals("reduceSubtreeHeightCase3Test...", expected, bt.toString());
    }

    private static void reduceSubtreeHeightCase2Test() {
        BalanceableTree<Integer> bt = new BalanceableTree<>();
        Node<Integer> n1 = bt.add(1);
        Node<Integer> n2 = bt.add(2);
        Node<Integer> n3 = bt.add(3);

        assertEquals("reduceSubtreeHeightCase2Test... root of subTree...", n2, bt.reduceSubtreeHeight(n3));
        String expected = "(2(1, 3))";
        assertEquals("reduceSubtreeHeightCase2Test...", expected, bt.toString());
    }

    private static void reduceSubtreeHeightCase1Test() {
        BalanceableTree<Integer> bt = new BalanceableTree<>();
        Node<Integer> n3 = bt.add(3);
        Node<Integer> n2 = bt.add(2);
        Node<Integer> n1 = bt.add(1);

        assertEquals("reduceSubtreeHeightCase4Test... root of subTree...", n2, bt.reduceSubtreeHeight(n1));
        String expected = "(2(1, 3))";
        assertEquals("reduceSubtreeHeightCase1Test...", expected, bt.toString());
    }

    private static void rotationTests() {
        BalanceableTree<Integer> bt = new BalanceableTree<>();
        Node<Integer> root = bt.add(8);
        Node<Integer> n3 = bt.add(3);
        Node<Integer> n1 = bt.add(1);
        Node<Integer> n6 = bt.add(6);
        Node<Integer> n4 = bt.add(4);
        Node<Integer> n7 = bt.add(7);
        Node<Integer> n10 = bt.add(10);
        Node<Integer> n14 = bt.add(14);
        Node<Integer> n13 = bt.add(13);
        String expected = "(8(3(1, 6(4, 7)), 10(_, 14(13, _))))";
        assertEquals("test BalanceableTree.toString()...", expected, bt.toString());

        bt.rotate(n3);
        String expected2 = "(3(1, 8(6(4, 7), 10(_, 14(13, _)))))";
        assertEquals("test rotate(Node)...", expected2, bt.toString());

        bt.rotateTwice(n6);
        String expected3 = "(6(3(1, 4), 8(7, 10(_, 14(13, _)))))";
        assertEquals("test rotateTwice(Node)...", expected3, bt.toString());
    }
}

