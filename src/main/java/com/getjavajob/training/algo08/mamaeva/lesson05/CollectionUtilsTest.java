package com.getjavajob.training.algo08.mamaeva.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo08.mamaeva.lesson05.CollectionUtils.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class CollectionUtilsTest {
    public static void main(String[] args) {
        List<Employee> listEmployee = new ArrayList<>();
        fillEmployeeList(listEmployee);

        filterTest(listEmployee);
        getTransformTest(listEmployee);
        transformTest(listEmployee);
        forAllDoTest(listEmployee);
        unmodifiableTest(listEmployee);
    }

    private static void unmodifiableTest(List<Employee> listEmployee) {
        Collection<Employee> unmodifiableList = unmodifiableCollection(listEmployee);
        assertEquals("test unmodifiableCollection...", listEmployee.toArray(), unmodifiableList.toArray());
        try {
            unmodifiableList.add(new Employee("", ""));
            fail("test unmodifiableList.add()...missed expected UOE");
        } catch (Exception e) {
            assertEquals("testunmodifiableList.add() with exception...", UnsupportedOperationException.class, e.getClass());
        }
    }

    private static void forAllDoTest(List<Employee> listEmployee) {
        List<Employee> expectedList = new ArrayList<>();
        fillExpList(expectedList);
        for (Employee element : expectedList) {
            element.increaseSalary(70_000);
        }

        Closure<Employee> employeeClosure = new Closure<Employee>() {
            @Override
            public void execute(Employee input) {
                input.increaseSalary(70_000);
            }
        };
        forAllDo(listEmployee, employeeClosure);
        assertEquals("test forAllDo...", expectedList.toArray(), listEmployee.toArray());
    }

    private static void transformTest(List<Employee> listEmployee) {
        List<Employee> transformatedList = new LinkedList<>(listEmployee);
        Transformer<Employee, String> transformer = new Transformer<Employee, String>() {
            @Override
            public String transform(Employee input) {
                return input.getLasName();
            }
        };
        transform(transformatedList, transformer);
        assertEquals("test transform...", lastNameList(listEmployee), transformatedList);
    }

    private static void getTransformTest(List<Employee> listEmployee) {
        Transformer<Employee, String> transformer = new Transformer<Employee, String>() {
            @Override
            public String transform(Employee input) {
                return input.getLasName();
            }
        };
        List<String> stringListEmployee = new ArrayList<>(getTransform(listEmployee, transformer));
        assertEquals("test getTransform...", lastNameList(listEmployee), stringListEmployee);
        assertEquals("test getTransform...", String.class, stringListEmployee.get(0).getClass());
    }

    private static void filterTest(List<Employee> listEmployee) {
        List<Employee> expectedList = new ArrayList<>();
        fillExpectedList(expectedList);

        Predicate<Employee> ivanovLastName = new Predicate<Employee>() {
            @Override
            public boolean apply(Employee element) {
                return "Ivanov".equals(element.getLasName());
            }
        };
        Collection<Employee> ivanovs = filter(listEmployee, ivanovLastName);
        assertEquals("test filter(target, predicate)...", expectedList, ivanovs);
    }

    private static List<String> lastNameList(List<Employee> list) {
        List<String> outputList = new ArrayList<>();
        for (Employee element : list) {
            outputList.add(element.getLasName());
        }
        return outputList;
    }

    private static void fillExpectedList(List<Employee> expectedList) {
        expectedList.add(new Employee("Ivanov", "Name1"));
        expectedList.add(new Employee("Ivanov", "Name2"));
        expectedList.add(new Employee("Ivanov", "Name3"));
    }

    private static void fillEmployeeList(List<Employee> listEmployee) {
        listEmployee.add(new Employee("Ivanov", "Name1"));
        listEmployee.add(new Employee("Portnov", "Name1"));
        listEmployee.add(new Employee("Petrov", "Name"));
        listEmployee.add(new Employee("Ivanov", "Name2"));
        listEmployee.add(new Employee("Lomakin", "Name"));
        listEmployee.add(new Employee("Karyakin", "Name"));
        listEmployee.add(new Employee("Ivanov", "Name3"));
    }

    private static void fillExpList(List<Employee> expectedList) {
        expectedList.add(new Employee("Ivanov", "Name1"));
        expectedList.add(new Employee("Portnov", "Name1"));
        expectedList.add(new Employee("Petrov", "Name"));
        expectedList.add(new Employee("Ivanov", "Name2"));
        expectedList.add(new Employee("Lomakin", "Name"));
        expectedList.add(new Employee("Karyakin", "Name"));
        expectedList.add(new Employee("Ivanov", "Name3"));
    }
}

class Employee {
    private String name;
    private String lasName;
    private int salary;

    public Employee(String lastName, String name) {
        this.name = name;
        this.lasName = lastName;
        salary = 0;
    }

    public int getSalary() {
        return salary;
    }

    public void increaseSalary(int incrrease) {
        salary = salary + incrrease;
    }

    public String getName() {
        return name;
    }

    public String getLasName() {
        return lasName;
    }

    @Override
    public boolean equals(Object e) {
        if (e instanceof String) {
            return e.equals(lasName + " " + name);
        } else {
            Employee emp = (Employee) e;
            return emp.name.equals(this.name) & emp.lasName.equals(this.lasName) & emp.salary == this.salary;
        }
    }

    @Override
    public String toString() {
        return name + " " + lasName + " " + salary;
    }
}
