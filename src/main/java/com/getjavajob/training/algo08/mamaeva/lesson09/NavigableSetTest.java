package com.getjavajob.training.algo08.mamaeva.lesson09;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.util.Assert.*;

public class NavigableSetTest {
    public static void main(String[] args) {
        NavigableSet<Integer> emptySet = new TreeSet<>();
        NavigableSet<Integer> set = new TreeSet<>();

        set.add(5);
        set.add(6);
        set.add(3);
        set.add(8);
        set.add(2);
        set.add(9);
        set.add(1);

        lowerTest(set);
        floorTest(set);
        ceilingTest(set);
        higherTest(set);
        pollFirstTest(emptySet, set);
        pollLastTest(emptySet, set);
        iteratorTest(set);
        assertEquals("test descendingSet()...", "[8, 6, 5, 3, 2]", set.descendingSet().toString());
        descendingIteratorTest(set);
        subSetTest(set);
        headSetTest(set);
        tailSetTest(set);

        printTestResult();
    }

    private static void tailSetTest(NavigableSet<Integer> set) {
        assertEquals("test tailSet()...", "[5, 6, 8]", set.tailSet(5, true).toString());
        try {
            System.out.println(set.tailSet(null, true));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test set.tailSet()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void headSetTest(NavigableSet<Integer> set) {
        assertEquals("test headSet()...", "[2, 3, 5]", set.headSet(5, true).toString());
        try {
            System.out.println(set.headSet(null, true));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test set.headSet()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void subSetTest(NavigableSet<Integer> set) {
        assertEquals("test subSet()...", "[3, 5, 6]", set.subSet(3, true, 8, false).toString());
        try {
            System.out.println(set.subSet(null, true, 15, false));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test set.subSet()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void descendingIteratorTest(NavigableSet<Integer> set) {
        Iterator dsc = set.descendingIterator();
        StringBuilder sb = new StringBuilder();
        while (dsc.hasNext()) {
            sb.append(dsc.next());
        }
        assertEquals("test descendingIterator()...", "86532", sb.toString());
    }

    private static void iteratorTest(NavigableSet<Integer> set) {
        Iterator iter = set.iterator();
        StringBuilder sb = new StringBuilder();
        while (iter.hasNext()) {
            sb.append(iter.next());
        }
        assertEquals("test iterator()...", "23568", sb.toString());
    }

    private static void pollLastTest(NavigableSet<Integer> emptySet, NavigableSet<Integer> set) {
        assertEquals("test pollLast()...", 9, (int) set.pollLast());
        assertEquals("test pollLast()...", "[2, 3, 5, 6, 8]", set.toString());
        assertEquals("test emptySet.pollLast()...", null, emptySet.pollFirst());
    }

    private static void pollFirstTest(NavigableSet<Integer> emptySet, NavigableSet<Integer> set) {
        assertEquals("test pollFirst()...", 1, (int) set.pollFirst());
        assertEquals("test pollFirst()...", "[2, 3, 5, 6, 8, 9]", set.toString());
        assertEquals("test emptySet.pollFirst()...", null, emptySet.pollFirst());
    }

    private static void higherTest(NavigableSet<Integer> set) {
        assertEquals("test set.higher(5)...", 6, (int) set.higher(5));
        assertEquals("test set.higher(10)...", null, set.higher(10));
        try {
            set.higher(null);
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test set.higher()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void ceilingTest(NavigableSet<Integer> set) {
        assertEquals("test set.ceiling(5)...", 5, (int) set.ceiling(4));
        assertEquals("test set.ceiling(10)...", null, set.ceiling(10));
        try {
            set.ceiling(null);
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test set.ceiling()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void floorTest(NavigableSet<Integer> set) {
        assertEquals("test set.floor(4)...", 3, (int) set.floor(4));
        assertEquals("test set.floor(3)...", 3, (int) set.floor(3));
        assertEquals("test set.floor(0)...", null, set.floor(0));
        try {
            set.floor(null);
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test set.floor()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void lowerTest(NavigableSet<Integer> set) {
        assertEquals("test set.lower(5)...", 3, (int) set.lower(5));
        assertEquals("test set.lower(0)...", null, set.lower(0));
        try {
            set.lower(null);
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test set.lower()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }
}
