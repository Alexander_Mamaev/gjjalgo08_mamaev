package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.*;

public class DynamicArrayListIteratorTest {
    private static List<Object> list = new ArrayList<>();
    private static DynamicArray dynamicArray = new DynamicArray();
    private static int countNewObject;
    private static DynamicArray.ListIteratorImpl daListIterator;
    private static ListIterator<Object> originalListIterator;

    public static void main(String[] args) {
        fillArrays();
        daListIterator = dynamicArray.listIterator();
        originalListIterator = list.listIterator();

        cursorEnd();
        testAdd("Test add(toEnd)...");
        testAdd("Test add(toEnd)...");
        testAdd("Test add(toEnd)...");
        stepPrevious();
        testRemove("Test removeHead(fromEnd)...");
        stepPrevious();
        testSet("test set(toEnd)...");
        testSet("test set(toEnd)...");
        cursorStart();
        testSet("Test set(toStart)...");
        testAdd("Test add(toStart)...");
        stepPrevious();
        testRemove("Test removeHead(fromStart)...");
        testAdd("Test add(toStart)...");

        for (int i = 0; i < 7; i++) {
            stepNext();
        }
        testRemove("Test removeHead(fromMiddle)...");
        stepNext();
        testAdd("Test add(toMiddle)...");
        stepNext();
        testSet("Test set(to Midlle)...");

        for (int i = 0; i < 5; i++) {
            stepPrevious();
        }
        testRemove("Test removeHead(fromMiddle)...");
        stepNext();
        testAdd("Test add(toMiddle)...");
        stepNext();
        testSet("Test set(to Midlle)...");
        stepNext();
        testAddCME("Test ConcurrentModificationException from add()...");
        testRemoveCME("Test ConcurrentModificationException from removeHead()...");
        testSetCME("Test ConcurrentModificationException from set()...");
        testNextCME("Test ConcurrentModificationException from next()...");
        testPreviousCME("Test ConcurrentModificationException from previous()...");

        printTestResult();
    }

    private static void testPreviousCME(String msg) {
        daListIterator = dynamicArray.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dynamicArray.add(getNewObject());
            dynamicArray.remove(dynamicArray.size() - 1);
            daListIterator.previous();
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testNextCME(String msg) {
        daListIterator = dynamicArray.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dynamicArray.add(getNewObject());
            dynamicArray.remove(dynamicArray.size() - 1);
            daListIterator.next();
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testSetCME(String msg) {
        daListIterator = dynamicArray.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dynamicArray.add(getNewObject());
            dynamicArray.remove(dynamicArray.size() - 1);
            daListIterator.set(getNewObject());
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testRemoveCME(String msg) {
        daListIterator = dynamicArray.listIterator();
        originalListIterator = list.listIterator();
        stepNext();
        try {
            dynamicArray.add(getNewObject());
            dynamicArray.remove(dynamicArray.size() - 1);
            daListIterator.remove();
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testAddCME(String msg) {
        try {
            dynamicArray.add(getNewObject());
            dynamicArray.remove(dynamicArray.size() - 1);
            daListIterator.add(getNewObject());
            fail("expected ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals(msg, ConcurrentModificationException.class, e.getClass());
        }
    }

    private static void testAdd(String msg) {
        Object newObject = getNewObject();
        originalListIterator.add(newObject);
        daListIterator.add(newObject);
        assertEquals(msg, list.toArray(), dynamicArray.toArray());
    }

    private static void testSet(String msg) {
        Object o = getNewObject();
        daListIterator.set(o);
        originalListIterator.set(o);
        assertEquals(msg, list.toArray(), dynamicArray.toArray());
    }

    private static void testRemove(String msg) {
        daListIterator.remove();
        originalListIterator.remove();
        assertEquals(msg, list.toArray(), dynamicArray.toArray());
    }

    //used for debugging
    private static void printArrays() {
        System.out.println("DA.nextIndex:" + daListIterator.nextIndex());
        System.out.print(Arrays.toString(dynamicArray.toArray()) + System.lineSeparator());
        System.out.println("list.nextIndex:" + originalListIterator.nextIndex());
        System.out.print(Arrays.toString(list.toArray()) + System.lineSeparator());
    }

    private static void cursorEnd() {
        while (daListIterator.hasNext()) {
            stepNext();
        }
    }

    private static void cursorStart() {
        while (originalListIterator.hasPrevious()) {
            stepPrevious();
        }
    }

    private static void stepNext() {
        daListIterator.next();
        originalListIterator.next();
    }

    private static void stepPrevious() {
        daListIterator.previous();
        originalListIterator.previous();
    }

    private static void fillArrays() {
        for (int i = 0; i < 15; i++) {
            String string = "Object" + i;
            dynamicArray.add(string);
            list.add(string);
        }
    }

    private static Object getNewObject() {
        return "newObject" + countNewObject++;
    }
}
