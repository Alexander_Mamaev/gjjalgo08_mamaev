9. will next (in txt file) statements compile?
1) byte b = 100 ?
2) byte b = 100L ?
3) int i = 100L ?
if not, write how to fix it
----------------------------------------------
1) byte b = 100 ... will compile.
2) byte b = 100L ... will not compile.
   The decision of this problem is:
   a) byte b = 100;
   b) byte b = (byte) 100L;
   c) long b = 100L;
3) int i = 100L ...will not compile.
   The decision of this problem is:
   a) int i = 100;
   b) int i = (int) 100L;
   c) long i = 100L;