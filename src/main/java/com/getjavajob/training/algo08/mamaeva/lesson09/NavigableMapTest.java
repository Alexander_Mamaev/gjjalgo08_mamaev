package com.getjavajob.training.algo08.mamaeva.lesson09;

import java.util.NavigableMap;
import java.util.TreeMap;

import static com.getjavajob.training.algo08.util.Assert.*;

public class NavigableMapTest {
    public static void main(String[] args) {
        NavigableMap<Integer, Integer> nmap = new TreeMap<>();
        NavigableMap<Integer, Integer> empty = new TreeMap<>();

        nmap.put(5, 5);
        nmap.put(6, 6);
        nmap.put(4, 4);
        nmap.put(7, 7);
        nmap.put(3, 3);
        nmap.put(8, 8);
        nmap.put(2, 2);
        nmap.put(9, 9);
        nmap.put(1, 1);

        lowerEntryTest(nmap);
        lowerKeyTest(nmap);
        floorEntryTest(nmap);
        floorKeyTest(nmap);
        ceilingEntryTest(nmap);
        ceilingKeyTest(nmap);
        higherEntryTest(nmap);
        higherKeyTest(nmap);
        firstEntryTest(nmap, empty);
        lastEntryTest(nmap, empty);
        pollFirstEntryTest(nmap, empty);
        pollLastEntryTest(nmap, empty);
        assertEquals("test descendingMap()...", "{8=8, 7=7, 6=6, 5=5, 4=4, 3=3, 2=2}", nmap.descendingMap().toString());
        assertEquals("test navigableKeySet()...", "[2, 3, 4, 5, 6, 7, 8]", nmap.navigableKeySet().toString());
        assertEquals("test descendingKeySet()...", "[8, 7, 6, 5, 4, 3, 2]", nmap.descendingKeySet().toString());
        subMapTest(nmap);
        headMapTest(nmap);
        tailMapTest(nmap);

        printTestResult();
    }

    private static void tailMapTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test tailMap()...", "{5=5, 6=6, 7=7, 8=8}", nmap.tailMap(5).toString());
        assertEquals("test tailMap()...", "{6=6, 7=7, 8=8}", nmap.tailMap(5, false).toString());
        try {
            System.out.println(nmap.tailMap(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test headMap()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void headMapTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test headMap()...", "{2=2, 3=3, 4=4}", nmap.headMap(5).toString());
        assertEquals("test headMap()...", "{2=2, 3=3, 4=4, 5=5}", nmap.headMap(5, true).toString());
        try {
            System.out.println(nmap.headMap(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test headMap()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void subMapTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test subMap()...", "{3=3, 4=4, 5=5}", nmap.subMap(3, 6).toString());
        assertEquals("test subMap()...", "{3=3, 4=4, 5=5, 6=6}", nmap.subMap(3, true, 6, true).toString());
        try {
            System.out.println(nmap.subMap(null, null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test subMap()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void pollLastEntryTest(NavigableMap<Integer, Integer> nmap, NavigableMap<Integer, Integer> empty) {
        assertEquals("test pollLastEntry()...", "9", nmap.pollLastEntry().getKey().toString());
        assertEquals("test pollLastEntry()...", "{2=2, 3=3, 4=4, 5=5, 6=6, 7=7, 8=8}", nmap.toString());
        assertEquals("test pollLastEntry()...", null, empty.pollLastEntry());
    }

    private static void pollFirstEntryTest(NavigableMap<Integer, Integer> nmap, NavigableMap<Integer, Integer> empty) {
        assertEquals("test pollFirstEntry()...", "1", nmap.pollFirstEntry().getKey().toString());
        assertEquals("test pollFirstEntry()...", "{2=2, 3=3, 4=4, 5=5, 6=6, 7=7, 8=8, 9=9}", nmap.toString());
        assertEquals("test pollFirstEntry()...", null, empty.pollFirstEntry());
    }

    private static void lastEntryTest(NavigableMap<Integer, Integer> nmap, NavigableMap<Integer, Integer> empty) {
        assertEquals("test lastEntry()...", "9=9", nmap.lastEntry().toString());
        assertEquals("test lastEntry()...", null, empty.lastEntry());
    }

    private static void firstEntryTest(NavigableMap<Integer, Integer> nmap, NavigableMap<Integer, Integer> empty) {
        assertEquals("test firstEntry()...", "1=1", nmap.firstEntry().toString());
        assertEquals("test firstEntry()...", null, empty.firstEntry());
    }

    private static void higherKeyTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test higherKey()...", "8", nmap.higherKey(7).toString());
        try {
            System.out.println(nmap.higherKey(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test higherKey()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void higherEntryTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test higherEntry()...", "8=8", nmap.higherEntry(7).toString());
        try {
            System.out.println(nmap.higherEntry(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test higherEntry()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void ceilingKeyTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test ceilingKey()...", "1", nmap.ceilingKey(-2).toString());
        assertEquals("test ceilingKey()...", "2", nmap.ceilingKey(2).toString());
        try {
            System.out.println(nmap.ceilingKey(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test ceilingKey()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void ceilingEntryTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test ceilingEntry()...", "1=1", nmap.ceilingEntry(-2).toString());
        assertEquals("test ceilingEntry()...", "2=2", nmap.ceilingEntry(2).toString());
        try {
            System.out.println(nmap.ceilingEntry(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test ceilingEntry()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void floorKeyTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test floorKey()...", "9", nmap.floorKey(15).toString());
        assertEquals("test floorKey()...", "9", nmap.floorKey(9).toString());
        try {
            System.out.println(nmap.floorKey(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test floorKey()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void floorEntryTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test floorEntry()...", "9=9", nmap.floorEntry(15).toString());
        assertEquals("test floorEntry()...", "9=9", nmap.floorEntry(9).toString());
        try {
            System.out.println(nmap.floorEntry(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test floorEntry()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void lowerKeyTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test lowerKey()...", "4", nmap.lowerKey(5).toString());
        try {
            System.out.println(nmap.lowerKey(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test lowerKey()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void lowerEntryTest(NavigableMap<Integer, Integer> nmap) {
        assertEquals("test lowerEntry()...", "4=4", nmap.lowerEntry(5).toString());
        try {
            System.out.println(nmap.lowerEntry(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test lowerEntry()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }
}
