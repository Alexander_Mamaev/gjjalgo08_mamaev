package com.getjavajob.training.algo08.mamaeva.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.*;

public class RedBlackTreeTest extends RedBlackTree {
    private static String expected;
    private static RedBlackTree<Integer> rbt;

    public static void main(String[] args) {
        testAdd();
        testRemove();
        randomTest();
        printTestResult();
    }

    private static void testRemove() {
        test1();
        test1m();
        test2();
        test2m();
        test3();
        test3m();
        test4();

        test20Remove19();
        test20Remove18();
        test20Remove17();
        test20Remove16();
        test20Remove15();
        test20Remove14();
        test20Remove13();
        test20remove12();
        test20Remove11();
        test20Remove10();
        test20Remove9();
        test20Remove8();
        test20Remove7();
        test20Remove6();
        test20Remove5();
        test20Remove4();
        test20Remove3();
        test20Remove2();
        test20Remove1();
        test20Remove0();

        testRemoveAllFromRoot();
    }

    private static void randomTest() {
        System.out.println();
        System.out.println("Random adding and deleting test (up to 100 elements), 100 passes");
        for (int j = 0; j < 100; j++) {
            rbt = new RedBlackTree<>();
            List<Integer> list = new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                int r = (int) (Math.random() * 100);
                rbt.add(r);
            }
            for (int i = 0; i < 10; i++) {
                int r = (int) (Math.random() * 100);
                list.add(r);
            }
            for (int i = 0; i < 300; i++) {
                int r = (int) (Math.random() * 100);
                if (!list.contains(r)) {
                    rbt.remove(r);
                }
            }
        }
        System.out.println(rbt.toColorString());
    }

    private static void testRemoveAllFromRoot() {
        rbt = rbt20();
        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(9(4(1(0, 2(_, 3)), 6(5, 8(7, _))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(8(4(1(0, 2(_, 3)), 6(5, 7)), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(7(4(1(0, 2(_, 3)), 6(5, _)), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(6(4(1(0, 2(_, 3)), 5), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(5(1(0, 3(2, 4)), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(4(1(0, 3(2, _)), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(3(1(0, 2), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(12(2(1(0, _), 11), 16(14(13, 15), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(11(1(0, 2), 16(14(13, 15), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(2(1(0, _), 16(14(13, 15), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(1(0, 16(14(13, 15), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(16(14(0(_, 13), 15), 18(17, 19)))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(15(13(0, 14), 18(17, 19)))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(14(13(0, _), 18(17, 19)))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(13(0, 18(17, 19)))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(18(0(_, 17), 19))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(17(0, 19))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(0(_, 19))";
        assertEquals("test.remove(0)", expected, rbt.toString());

        rbt.remove(rbt.root());
        System.out.println(rbt.toColorString());
        expected = "(19)";
        assertEquals("test.remove(0)", expected, rbt.toString());

        try {
            rbt.remove(rbt.root());
            System.out.println(rbt.toColorString());
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test.remove(only root)... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void test20Remove0() {
        rbt = rbt20();
        rbt.remove(0);
        System.out.println(rbt.toColorString());
        expected = "(10(4(2(1, 3), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(0)", expected, rbt.toString());
    }

    private static void test20Remove1() {
        rbt = rbt20();
        rbt.remove(1);
        System.out.println(rbt.toColorString());
        expected = "(10(4(2(0, 3), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(1)", expected, rbt.toString());
    }

    private static void test20Remove2() {
        rbt = rbt20();
        rbt.remove(2);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 3), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(2)", expected, rbt.toString());
    }

    private static void test20Remove3() {
        rbt = rbt20();
        rbt.remove(3);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(3)", expected, rbt.toString());
    }

    private static void test20Remove4() {
        rbt = rbt20();
        rbt.remove(4);
        System.out.println(rbt.toColorString());
        expected = "(10(3(1(0, 2), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(4)", expected, rbt.toString());
    }

    private static void test20Remove5() {
        rbt = rbt20();
        rbt.remove(5);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 8(6(_, 7), 9)), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(5)", expected, rbt.toString());
    }

    private static void test20Remove6() {
        rbt = rbt20();
        rbt.remove(6);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 8(5(_, 7), 9)), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(6)", expected, rbt.toString());
    }

    private static void test20Remove7() {
        rbt = rbt20();
        rbt.remove(7);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(_, 9))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(7)", expected, rbt.toString());
    }

    private static void test20Remove8() {
        rbt = rbt20();
        rbt.remove(8);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 7(_, 9))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(8)", expected, rbt.toString());
    }

    private static void test20Remove9() {
        rbt = rbt20();
        rbt.remove(9);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, _))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(9)", expected, rbt.toString());
    }

    private static void test20Remove10() {
        rbt = rbt20();
        rbt.remove(10);
        System.out.println(rbt.toColorString());
        expected = "(9(4(1(0, 2(_, 3)), 6(5, 8(7, _))), 16(12(11, 14(13, 15)), 18(17, 19))))";
        assertEquals("test.remove(10)", expected, rbt.toString());
    }

    private static void test20Remove11() {
        rbt = rbt20();
        rbt.remove(11);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(14(12(_, 13), 15), 18(17, 19))))";
        assertEquals("test.remove(11)", expected, rbt.toString());
    }

    private static void test20remove12() {
        rbt = rbt20();
        rbt.remove(12);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(14(11(_, 13), 15), 18(17, 19))))";
        assertEquals("test.remove(12)", expected, rbt.toString());
    }

    private static void test20Remove13() {
        rbt = rbt20();
        rbt.remove(13);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(12(11, 14(_, 15)), 18(17, 19))))";
        assertEquals("test.remove(13)", expected, rbt.toString());
    }

    private static void test20Remove14() {
        rbt = rbt20();
        rbt.remove(14);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(12(11, 13(_, 15)), 18(17, 19))))";
        assertEquals("test.remove(14)", expected, rbt.toString());
    }

    private static void test20Remove15() {
        rbt = rbt20();
        rbt.remove(15);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(12(11, 14(13, _)), 18(17, 19))))";
        assertEquals("test.remove(15)", expected, rbt.toString());
    }

    private static void test20Remove16() {
        rbt = rbt20();
        rbt.remove(16);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 15(12(11, 14(13, _)), 18(17, 19))))";
        assertEquals("test.remove(16)", expected, rbt.toString());
    }

    private static void test20Remove17() {
        rbt = rbt20();
        rbt.remove(17);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 18(_, 19))))";
        assertEquals("test.remove(17)", expected, rbt.toString());
    }

    private static void test20Remove18() {
        rbt = rbt20();
        rbt.remove(18);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 17(_, 19))))";
        assertEquals("test.remove(18)", expected, rbt.toString());
    }

    private static void test20Remove19() {
        rbt = rbt20();
        rbt.remove(19);
        System.out.println(rbt.toColorString());
        expected = "(10(4(1(0, 2(_, 3)), 6(5, 8(7, 9))), 16(12(11, 14(13, 15)), 18(17, _))))";
        assertEquals("test.remove(19)", expected, rbt.toString());
    }

    private static RedBlackTree<Integer> rbt20() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        rbt.add(19);
        rbt.add(16);
        rbt.add(10);
        rbt.add(5);
        rbt.add(12);
        rbt.add(0);
        rbt.add(15);
        rbt.add(4);
        rbt.add(6);
        rbt.add(8);
        rbt.add(1);
        rbt.add(7);
        rbt.add(9);
        rbt.add(18);
        rbt.add(2);
        rbt.add(3);
        rbt.add(11);
        rbt.add(13);
        rbt.add(14);
        rbt.add(17);
        return rbt;
    }

    private static void test4() {
        rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(10);
        rbt.add(30);
        rbt.add(50);
        rbt.add(70);
        rbt.add(25);
        rbt.add(55);

        rbt.remove(60);
        System.out.println(rbt.toColorString());
        expected = "(40(20(10, 30(25, _)), 55(50, 70)))";
        assertEquals("test4", expected, rbt.toString());
    }

    private static void test3m() {
        //test3 - mirrored
        rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(10);
        rbt.add(30);

        rbt.remove(20);
        System.out.println(rbt.toColorString());
        expected = "(40(10(_, 30), 60))";
        assertEquals("test3m", expected, rbt.toString());
    }

    private static void test3() {
        //test3
        rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(50);
        rbt.add(70);

        rbt.remove(60);
        System.out.println(rbt.toColorString());
        expected = "(40(20, 50(_, 70)))";
        assertEquals("test3", expected, rbt.toString());
    }

    private static void test2m() {
        //test2 - mirrored
        rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(50);
        rbt.add(70);

        rbt.remove(50);
        rbt.remove(70);
        System.out.println(rbt.toColorString());
        expected = "(40(20, 60))";
        assertEquals("test2m", expected, rbt.toString());
    }

    private static void test2() {
        //test2
        rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(10);
        rbt.add(30);

        rbt.remove(10);
        rbt.remove(30);
        System.out.println(rbt.toColorString());
        expected = "(40(20, 60))";
        assertEquals("test2", expected, rbt.toString());
    }

    private static void test1m() {
        //test1 - mirrored
        rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(10);
        rbt.add(30);

        rbt.remove(60);
        System.out.println(rbt.toColorString());
        expected = "(20(10, 40(30, _)))";
        assertEquals("test1m", expected, rbt.toString());
    }

    private static void test1() {
        rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(50);
        rbt.add(70);

        rbt.remove(20);
        System.out.println(rbt.toColorString());
        expected = "(60(40(_, 50), 70))";
        assertEquals("test1", expected, rbt.toString());
    }

    private static void testAdd() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        rbt.add(40);
        rbt.add(20);
        rbt.add(60);
        rbt.add(30);
        rbt.add(50);
        rbt.add(10);
        rbt.add(70);
        rbt.add(35);
        rbt.add(45);
        rbt.add(55);
        rbt.add(57);
        rbt.add(65);
        rbt.add(63);
        rbt.add(58);
        rbt.add(59);
        rbt.add(62);
        rbt.add(28);
        rbt.add(24);
        rbt.add(23);
        rbt.add(29);

        System.out.println(rbt.toColorString());
        for (Node<Integer> n : rbt.inOrder()) {
            System.out.print(n.getElement() + "/");
        }
    }
}

