package com.getjavajob.training.algo08.mamaeva.lesson01;

import static com.getjavajob.training.algo08.mamaeva.lesson01.Task06.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class Task06Test {
    public static void main(String[] args) {
        int a = 0b1010101; //85
        testTwoPowN(4, 0b10000);
        testTwoPowNPlusTwoPowM(2, 3, 0b1100);
        testTwoPowNPlusTwoPowM(3, 3, 0b10000);
        testResetLowerBits(a, 4, 0b1010000);
        testSetBitOne(a, 4, 0b1011101);
        testInvertBit(a, 4, 0b1011101);
        testSetBitZero(a, 3, 0b1010001);
        testGetLowerBits(a, 4, 0b101);
        testGetBit(a, 3, 0b1);
        testGetBit(a, 4, 0b0);
        testByteToBinaryString(-a, "10101011");
        testByteToBinaryString(a, Integer.toBinaryString(a));
        printTestResult();
    }

    private static void testTwoPowN(int n, int expectedResult) {
        System.out.printf("TaskA: 2^n (n=4) --> 0b%s (%s)%n", Integer.toBinaryString(twoPowN(n)), twoPowN(n));
        assertEquals("TaskA_testTwoPowN:", expectedResult, twoPowN(n));
        System.out.println();
    }

    private static void testTwoPowNPlusTwoPowM(int n, int m, int expectedResult) {
        System.out.printf("TaskB: 2^n + 2^m (n=%s, m=%s) --> 0b%s (%s)%n", n, m, Integer.toBinaryString(twoPowNPlusTwoPowM(n, m)), twoPowNPlusTwoPowM(n, m));
        assertEquals("TaskB_testTwoPowNPlusTwoPowM:", expectedResult, twoPowNPlusTwoPowM(n, m));
        System.out.println();
    }

    private static void testResetLowerBits(int a, int n, int expectedResult) {
        System.out.printf("TaskC: %s with reset %s lower bits --> %s%n", Integer.toBinaryString(a), n, Integer.toBinaryString(resetLowerBits(a, n)));
        assertEquals("TaskC_testResetLowerBits:", expectedResult, resetLowerBits(a, n));
        System.out.println();
    }

    private static void testSetBitOne(int a, int n, int expectedResult) {
        System.out.printf("TaskD: set %s %s-th bit with 1 --> %s%n", Integer.toBinaryString(a), n, Integer.toBinaryString(setBitOne(a, n)));
        assertEquals("TaskD_testSetBitOne:", expectedResult, setBitOne(a, n));
        System.out.println();
    }

    private static void testInvertBit(int a, int n, int expectedResult) {
        System.out.printf("TaskE: %s with invert %s-th bit --> %S%n", Integer.toBinaryString(a), n, Integer.toBinaryString(invertBit(a, n)));
        assertEquals("TaskE_testInvertBit:", expectedResult, invertBit(a, n));
        System.out.println();
    }

    private static void testSetBitZero(int a, int n, int expectedResult) {
        System.out.printf("TaskF: set %s %s-th bit with 0 --> %s%n", Integer.toBinaryString(a), n, Integer.toBinaryString(setBitZero(a, n)));
        assertEquals("TaskF_testSetBitZero:", expectedResult, setBitZero(a, n));
        System.out.println();
    }

    private static void testGetLowerBits(int a, int n, int expectedResult) {
        System.out.printf("TaskG: return %s lower bits from %s --> %s%n", n, Integer.toBinaryString(a), Integer.toBinaryString(getLowerBits(a, n)));
        assertEquals("TaskG_testGetLowerBits:", expectedResult, getLowerBits(a, n));
        System.out.println();
    }

    private static void testGetBit(int a, int n, int expectedResult) {
        System.out.printf("TaskH: return %s-th bit from %s --> %s%n", n, Integer.toBinaryString(a), Integer.toBinaryString(getBit(a, n)));
        assertEquals("TaskH_testGetBit:", expectedResult, getBit(a, n));
        System.out.println();
    }

    private static void testByteToBinaryString(int a, String expectedResult) {
        System.out.printf("TaskI: output bin representation of byte %s (0b%s) --> %s%n", (byte) a, Integer.toBinaryString(a), byteToBinaryString((byte) a));
        assertEquals("TaskI_testByteToBinaryString:", expectedResult, byteToBinaryString((byte) a));
        System.out.println();
    }
}
