package com.getjavajob.training.algo08.mamaeva.lesson07.tree.binary;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {
    private List<Node<E>> list = new ArrayList<>();
    private int size;

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = (NodeImpl<E>) p;
        int leftIndex = getLeftIndex(node);
        if (leftIndex > list.size() - 1) {
            return null;
        }
        return list.get(leftIndex);
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = (NodeImpl<E>) p;
        int rightIndex = getRightIndex(node);
        if (rightIndex > list.size() - 1) {
            return null;
        }
        return list.get(rightIndex);
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = (NodeImpl<E>) n;
        int leftIndex = getLeftIndex(node);
        if (leftIndex < list.size() && list.get(leftIndex).getElement() != null) {
            throw new IllegalArgumentException("the left child is already exist");
        }
        return addNode(e, leftIndex);
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = (NodeImpl<E>) n;
        int rightIndex = getRightIndex(node);
        if (rightIndex < list.size() && list.get(rightIndex).getElement() != null) {
            throw new IllegalArgumentException("the right child is already exist");
        }
        return addNode(e, rightIndex);
    }

    private Node<E> addNode(E e, int index) {
        if (index > list.size() - 1) {
            enhanceList();
        }
        NodeImpl<E> nodeAdded = new NodeImpl<>(e, index);
        list.set(index, nodeAdded);
        size++;
        return nodeAdded;
    }

    private void enhanceList() {
        int diff = list.size() + 1;
        for (int i = 0; i < diff; i++) {
            list.add(new NodeImpl<E>(null, list.size()));
        }
    }

    private int getLeftIndex(NodeImpl<E> node) {
        return 2 * node.index + 1;
    }

    private int getRightIndex(NodeImpl<E> node) {
        return 2 * node.index + 2;
    }

    @Override
    public Node<E> root() {
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = (NodeImpl<E>) n;
        int childIndex = node.index;
        int parentIndex;
        if (childIndex == 0) {
            return null;
        } else if (childIndex % 2 == 0) {
            parentIndex = (childIndex - 2) / 2;
        } else {
            parentIndex = (childIndex - 1) / 2;
        }
        return list.get(parentIndex);
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (list.size() != 0) {
            throw new IllegalStateException("the root is already exist");
        }
        NodeImpl<E> nodeAdded = new NodeImpl<>(e, 0);
        list.add(nodeAdded);
        size++;
        return nodeAdded;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = (NodeImpl<E>) n;
        Node<E> returnedNode;
        int leftIndex = getLeftIndex(node);
        int rightIndex = getRightIndex(node);
        if (rightIndex > list.size() - 1) {
            enhanceList();
        }
        if (list.get(leftIndex).getElement() == null) {
            returnedNode = addLeft(node, e);
        } else if (list.get(rightIndex).getElement() == null) {
            returnedNode = addRight(node, e);
        } else {
            throw new IllegalArgumentException("the node has left and right child already");
        }
        return returnedNode;
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("this node is null");
        }
        NodeImpl<E> node = (NodeImpl<E>) n;
        E replacedElement = node.element;
        node.element = e;
        return replacedElement;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("this node is null already");
        }
        NodeImpl<E> node = (NodeImpl<E>) n;
        E replacedElement = node.element;
        removeChildesOf(node);
        node.element = null;
        size--;
        return replacedElement;
    }

    private void removeChildesOf(Node<E> n) {
        NodeImpl<E> node = (NodeImpl<E>) n;
        if (getLeftIndex(node) < list.size()) {
            NodeImpl<E> leftChild = (NodeImpl<E>) left(node);
            removeChildesOf(leftChild);
            leftChild.element = null;
            size--;
        }
        if (getRightIndex(node) < list.size()) {
            NodeImpl<E> rightChild = (NodeImpl<E>) right(node);
            removeChildesOf(rightChild);
            rightChild.element = null;
            size--;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return super.iterator();
    }

    @Override
    public Iterable<Node<E>> nodes() {
        return list;
    }

    protected static class NodeImpl<E> implements Node<E> {
        private E element;
        private int index;

        public NodeImpl(E element, int index) {
            this.element = element;
            this.index = index;
        }

        @Override
        public E getElement() {
            return element;
        }

        @Override
        public String toString() {
            return "NodeImpl{" +
                    "element=" + element +
                    ", index=" + index +
                    '}';
        }
    }
}