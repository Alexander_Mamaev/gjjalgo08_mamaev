package com.getjavajob.training.algo08.mamaeva.lesson04;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class DoublyLinkedListPerformanceTest {
    private static final String GREEN = "\u001B[32m";
    private static final String DEFAULT = "\u001B[0m";

    private static StopWatch time = new StopWatch();
    private static DoublyLinkedList<Object> dLL;
    private static List<Object> list;
    private static HashMap<String, Long> result = new HashMap<>();

    public static void main(String[] args) {
        dLLAddRemoveBeginTest(200_000_000);
        linkedListAddRemoveBeginTest(200_000_000);
        dLLAddRemoveMiddleTest(200_000);
        linkedListAddRemoveMiddleTest(200_000);
        dLLAddRemoveEndTest(200_000_000);
        linkedListAddRemoveEndTest(200_000_000);
        System.out.println(GREEN + result.toString() + DEFAULT);
    }

    private static void linkedListAddRemoveEndTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        list = new LinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestLLAddRemoveEnd(size);
            totalRemoveTime += timeTestLLRemoveEnd(list.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average linkedList.add(toEnd) time", totalAddTime / iter);
        result.put("Average linkedList.removeHead(fromEnd) time", totalRemoveTime / iter);
    }

    private static void dLLAddRemoveEndTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        dLL = new DoublyLinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestDLLAddEnd(size);
            totalRemoveTime += timeTestDLLRemovedEnd(dLL.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average dLL.add(toEnd) time", totalAddTime / iter);
        result.put("Average dLL.removeHead(fromEnd) time", totalRemoveTime / iter);
    }

    private static void linkedListAddRemoveMiddleTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        list = new LinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestLLAddMiddle(size);
            totalRemoveTime += timeTestLLRemovedMiddle(list.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average linkedList.add(toMiddle) time", totalAddTime / iter);
        result.put("Average linkedList.removeHead(fromMiddle) time", totalRemoveTime / iter);
    }

    private static void dLLAddRemoveMiddleTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        dLL = new DoublyLinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestDLLAddMiddle(size);
            totalRemoveTime += timeTestDLLRemoveMiddle(dLL.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average dLL.add(toMiddle) time", totalAddTime / iter);
        result.put("Average dLL.removeHead(fromMiddle) time", totalRemoveTime / iter);
    }

    private static void dLLAddRemoveBeginTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        dLL = new DoublyLinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestDLLAddBegin(size);
            totalRemoveTime += timeTestDLLRemovedBegin(dLL.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average dLL.add(toBegin) time", totalAddTime / iter);
        result.put("Average dLL.removeHead(fromBegin) time", totalRemoveTime / iter);
    }

    private static void linkedListAddRemoveBeginTest(int size) {
        long totalAddTime = 0;
        long totalRemoveTime = 0;
        long iter = 10;
        list = new LinkedList<>();
        for (int i = 0; i < iter; i++) {
            System.gc();
            System.out.println(GREEN + "-------------------Start----------------------");
            totalAddTime += timeTestLLAddBegin(size);
            totalRemoveTime += timeTestLLRemovedBegin(list.size());
            System.out.println("-------------------Stop-----------------------" + DEFAULT);
        }
        result.put("Average linkedList.add(toBegin) time", totalAddTime / iter);
        result.put("Average linkedList.removeHead(fromBegin) time", totalRemoveTime / iter);
    }

    private static long timeTestDLLAddBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dLL.add("");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("DoublyLinkedList.add(toBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestDLLRemovedBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dLL.remove(0);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("DoublyLinkedList.removed(fromBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLAddBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.add("");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.add(toBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLRemovedBegin(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.remove(0);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.removeHead(fromBegin): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestDLLAddMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dLL.add(dLL.size() >> 1, "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("DoublyLinkedList.add(toMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestDLLRemoveMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dLL.remove(dLL.size() >> 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("DoublyLinkedList.removeHead(fromMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLAddMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.add(list.size() >> 1, "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.add(toMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLRemovedMiddle(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.remove(list.size() >> 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.removeHead(fromMiddle): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestDLLAddEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dLL.add(dLL.size(), "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("DoublyLinkedList.add(toEnd): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestDLLRemovedEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dLL.remove(dLL.size() - 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("DoublyLinkedList.removeHead(fromEnd): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLAddRemoveEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.add(list.size(), "");
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.add(toEnd): " + totalTime + " ms");
        return totalTime;
    }

    private static long timeTestLLRemoveEnd(int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.remove(list.size() - 1);
        }
        long totalTime = time.getElapsedTime();
        System.out.println("LinkedList.removeHead(fromEnd): " + totalTime + " ms");
        return totalTime;
    }
}
