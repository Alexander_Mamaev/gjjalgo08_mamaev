package com.getjavajob.training.algo08.mamaeva.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * class ExpressionCalculator, that will convert arithmetic expression written in infix notation
 * to postfix notation (aka reverse polish notation) and then calculate expression's result.
 * expressions can have binary operators (arity 2): + - / * and ( )
 */
public class ExpressionCalculator {
    /**
     * Calculate expression
     *
     * @param ex - expression in infix notation which can have binary operators (arity 2): + - / * and ( )
     * @return result of calculation
     * @throws NoSuchMethodException
     */
    public static double calculatePostfix(String ex) throws NoSuchMethodException {
        Deque<String> stack = new ArrayDeque<>();
        Deque<String> input = new ArrayDeque<>(convertInfixToPostfix(ex));
        while (!input.isEmpty()) {
            String token = input.pop();
            if (isOperator(token)) {
                stack.push(String.valueOf(calculatePair(stack.pop(), stack.pop(), token)));
            } else {
                stack.push(token);
            }
        }
        return Double.parseDouble(stack.pop());
    }

    private static double calculatePair(String second, String first, String operator) throws NoSuchMethodException {
        double a = Double.parseDouble(first);
        double b = Double.parseDouble(second);
        switch (operator) {
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return a * b;
            case "/":
                return a / b;
            default:
                throw new NoSuchMethodException("not found such operator realization.");
        }
    }

    /**
     * convert expression with infix notation to postfix notation for next calculation.
     *
     * @param ex - Expression in infix notation which can have binary operators (arity 2): + - / * and ( )
     * @return stack of expression tokens with postfix notation sequence
     */
    public static Deque<String> convertInfixToPostfix(String ex) {
        Deque<String> output = new ArrayDeque<>();
        Deque<String> stack = new ArrayDeque<>();
        Deque<String> exTokens = convertStringToTokens(ex);
        while (!exTokens.isEmpty()) {
            String token = exTokens.pop();
            if (isOperator(token)) {
                if (stack.isEmpty() || priority(token) > priority(stack.peek())) {
                    stack.push(token);
                } else {
                    while (!stack.isEmpty() && !isParentheses(stack.peek())) {
                        output.addLast(stack.pop());
                    }
                    stack.push(token);
                }
            } else if (isParentheses(token)) {
                if ("(".equals(token)) {
                    stack.push(token);
                } else {
                    while (!stack.isEmpty()) {
                        if ("(".equals(stack.peek())) {
                            stack.pop();
                            break;
                        }
                        output.addLast(stack.pop());
                    }
                }
            } else {
                output.addLast(token);
            }
        }
        while (!stack.isEmpty()) {
            output.addLast(stack.pop());
        }
        return output;
    }

    private static Deque<String> convertStringToTokens(String ex) {
        Deque<String> output = new ArrayDeque<>();
        StringBuilder token = new StringBuilder();
        int exSize = ex.length();
        for (int i = 0; i < exSize; i++) {
            String element = String.valueOf(ex.charAt(i));
            if (priority(element) == 0) {
                token.append(element);
            } else if (token.length() > 0) {
                output.addLast(token.toString());
                output.addLast(element);
                token = token.delete(0, token.length());
            } else {
                output.addLast(element);
            }
        }
        if (token.length() > 0) {
            output.addLast(token.toString());
        }
        return output;
    }

    private static boolean isParentheses(String token) {
        return priority(token) == 1;
    }

    private static boolean isOperator(String token) {
        return priority(token) >= 2;
    }

    private static byte priority(String token) {
        switch (token) {
            case "*":
            case "/":
                return 3;
            case "+":
            case "-":
                return 2;
            case "(":
            case ")":
                return 1;
            default:
                return 0;
        }
    }
}
