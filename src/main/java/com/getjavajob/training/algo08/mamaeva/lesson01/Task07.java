package com.getjavajob.training.algo08.mamaeva.lesson01;

public class Task07 {
    public static int[] swapNumbers1(int a, int b) {
        a += b;
        b = a - b;
        a -= b;
        return new int[]{a, b};
    }

    /**
     * @param a non Zero
     * @param b non Zero
     * @return swap numbers
     */
    public static int[] swapNumbers2(int a, int b) {
        a *= b;
        b = a / b;
        a /= b;
        return new int[]{a, b};
    }

    /**
     * @param a != b
     * @param b != a
     * @return swap numbers
     */
    public static int[] swapNumbers3(int a, int b) {
        a ^= b;
        b ^= a;
        a ^= b;
        return new int[]{a, b};
    }

    /**
     * @param a 15 bit number at maximum
     * @param b 15 bit number at maximum
     * @return swap numbers
     */
    public static int[] swapNumbers4(int a, int b) {
        a <<= 16;
        b <<= 16;
        b >>>= 16;
        a |= b;
        b = a >> 16;
        a <<= 16;
        a >>= 16;
        return new int[]{a, b};
    }
}
