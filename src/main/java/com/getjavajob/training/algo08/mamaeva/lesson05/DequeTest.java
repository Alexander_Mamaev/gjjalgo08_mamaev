package com.getjavajob.training.algo08.mamaeva.lesson05;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.*;

public class DequeTest {
    private static final Deque<Object> deque = new ArrayDeque<>();
    private static final Deque<Object> emptyDeque = new ArrayDeque<>();
    private static final List<Object> list = new ArrayList<>();
    private static int countObjects;

    public static void main(String[] args) {
        addFirstTest();
        addLastTest();
        offerFirstTest();
        offerLastTest();
        removeFirstTest();
        removeLastTest();
        pollFirstTest();
        pollLastTest();

        assertEquals("test deque.getFirst()...", list.get(0), deque.getFirst());
        assertEquals("test deque.getLast()...", list.get(list.size() - 1), deque.getLast());
        assertEquals("test deque.peekFirst()...", list.get(0), deque.peekFirst());
        assertEquals("test emptyDeque.peekFirst()...", null, emptyDeque.peekFirst());
        assertEquals("test deque.peekLast()...", list.get(list.size() - 1), deque.peekLast());
        assertEquals("test emptyDeque.peekLast()...", null, emptyDeque.peekLast());

        List<Object> copyList = list;
        deque.addAll(copyList);
        list.addAll(copyList);
        removeFirstOccurrenceTest();
        removeLastOccurrenceTest();
        addTest();
        offerTest();
        removeTest();
        pollTest();
        elementTest();
        peekTest();
        pushTest();
        popTest();
        removeObjectTest();

        assertEquals("test deque.contains(o)...", true, deque.contains("Object0"));
        assertEquals("test deque.contains(o)...", false, deque.contains(""));
        assertEquals("test deque.size()...", list.size(), deque.size());
        assertEquals("test deque.iterator()...", "java.util.ArrayDeque$DeqIterator", deque.iterator().getClass().getName());
        assertEquals("test deque.descendingIterator()...", "java.util.ArrayDeque$DescendingIterator", deque.descendingIterator().getClass().getName());
        System.out.println(deque.descendingIterator().getClass().getName());

        printTestResult();
    }

    private static void removeObjectTest() {
        Object o = "object5";
        assertEquals("test deque.remove(o)...", list.remove(o), deque.remove(o));
        assertEquals("test deque.remove(o)...", list.toArray(), deque.toArray());
    }

    private static void popTest() {
        assertEquals("test deque.pop()...", list.get(0), deque.pop());
        list.remove(0);
        assertEquals("test deque.poop()...", list.toArray(), deque.toArray());
        try {
            emptyDeque.pop();
            fail("test emptyDeque.pop() with empty queue...missed expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test emptyDeque.pop() with empty queue with exception...", NoSuchElementException.class, e.getClass());
        }
    }

    private static void pushTest() {
        Object o = getNewObject();
        deque.push(o);
        list.add(0, o);
        assertEquals("test deque.push(o)...", list.toArray(), list.toArray());
    }

    private static void peekTest() {
        assertEquals("test deque.peek()...", list.get(0), deque.peek());
        assertEquals("test deque.peek()...", list.toArray(), deque.toArray());
        assertEquals("test emptyDeque.peek()...", null, emptyDeque.peek());
    }

    private static void elementTest() {
        assertEquals("test deque.element()...", list.get(0), deque.element());
        assertEquals("test deque.element()...", list.toArray(), deque.toArray());
        try {
            emptyDeque.element();
            fail("test emptyDeque.poll() with empty queue...missed expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test emptyDeque.poll() with empty queue with exception...", NoSuchElementException.class, e.getClass());
        }
    }

    private static void pollTest() {
        assertEquals("test deque.poll()...", list.get(0), deque.poll());
        list.remove(0);
        assertEquals("test emptyDeque.poll()...", null, emptyDeque.poll());
        assertEquals("test deque.poll()...", list.toArray(), deque.toArray());
    }

    private static void removeTest() {
        assertEquals("test deque.remove()...", list.get(0), deque.remove());
        list.remove(0);
        assertEquals("test deque.remove()...", list.toArray(), deque.toArray());
        try {
            emptyDeque.remove();
            fail("test deque.remove() with empty queue...missed expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test deque.remove() with empty queue with exception...", NoSuchElementException.class, e.getClass());
        }
    }

    private static void offerTest() {
        Object o = getNewObject();
        assertEquals("test deque.offer(o)...", list.add(o), deque.offer(o));
        assertEquals("test deque.offer(o)...", list.toArray(), deque.toArray());
    }

    private static void addTest() {
        Object o = getNewObject();
        assertEquals("test deque.add(o)...", list.add(o), deque.add(o));
        assertEquals("test deque.add(o)...", list.toArray(), deque.toArray());
    }

    private static void removeLastOccurrenceTest() {
        Object o = "Object1";
        assertEquals("test deque.removeLastOccurrence(o)...", true, deque.removeLastOccurrence(o));
        list.remove(7);
        assertEquals("test deque.removeLastOccurrence(o)...", false, deque.removeLastOccurrence(""));
        assertEquals("test deque.removeLastOccurrence(o)...", list.toArray(), deque.toArray());
    }

    private static void removeFirstOccurrenceTest() {
        Object o = "Object0";
        assertEquals("test deque.removeFirstOccurrence(o)...", list.remove(o), deque.removeFirstOccurrence(o));
        assertEquals("test deque.removeFirstOccurrence(o)...", false, deque.removeFirstOccurrence(""));
        assertEquals("test deque.removeFirstOccurrence(o)...", list.toArray(), deque.toArray());
    }

    private static void pollLastTest() {
        assertEquals("test deque.pollLast()...", list.get(list.size() - 1), deque.pollLast());
        list.remove(list.size() - 1);
        assertEquals("test deque.pollLast()...", list.toArray(), deque.toArray());
        assertEquals("test emptyDeque.pollLast()...", null, emptyDeque.pollLast());
    }

    private static void pollFirstTest() {
        assertEquals("test deque.pollFirst()...", list.get(0), deque.pollFirst());
        list.remove(0);
        assertEquals("test deque.pollFirst()...", list.toArray(), deque.toArray());
        assertEquals("test emptyDeque.pollFirst()...", null, emptyDeque.pollFirst());
    }

    private static void removeLastTest() {
        assertEquals("test deque.removeLast()...", list.get(list.size() - 1), deque.removeLast());
        list.remove(list.size() - 1);
        assertEquals("test deque.removeLast()...", list.toArray(), deque.toArray());
    }

    private static void removeFirstTest() {
        assertEquals("test deque.removeFirst()...", list.get(0), deque.removeFirst());
        list.remove(0);
        assertEquals("test deque.removeFirst()...", list.toArray(), deque.toArray());
    }

    private static void offerLastTest() {
        Object o = getNewObject();
        list.add(o);
        assertEquals("test deque.offerLast(o)...", true, deque.offerLast(o));
        assertEquals("test deque.offerLast(o)...", list.toArray(), deque.toArray());
    }

    private static void offerFirstTest() {
        Object o = getNewObject();
        list.add(0, o);
        assertEquals("test deque.offerFirst(o)...", true, deque.offerFirst(o));
        assertEquals("test deque.offerFirst(o)...", list.toArray(), deque.toArray());
    }

    private static void addLastTest() {
        for (int i = 0; i < 3; i++) {
            Object o = getNewObject();
            deque.addLast(o);
            list.add(list.size(), o);
        }
        assertEquals("test deque.addLast(o)...", list.toArray(), deque.toArray());
    }

    private static void addFirstTest() {
        for (int i = 0; i < 5; i++) {
            Object o = getNewObject();
            deque.addFirst(o);
            list.add(0, o);
        }
        assertEquals("test deque.addFirst(o)...", list.toArray(), deque.toArray());
    }

    private static String getNewObject() {
        return "Object" + countObjects++;
    }
}
