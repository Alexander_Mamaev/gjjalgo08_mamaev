package com.getjavajob.training.algo08.mamaeva.lesson06;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedHashMapTest {
    private static int countObjects;

    public static void main(String[] args) {
        Map<String, String> map = new LinkedHashMap<>();
        String expectedLinkedHashMapOrder = "{key_0=value_0, key_1=value_1, key_2=value_2, key_3=value_3, key_4=value_4, key_5=value_5, key_6=value_6, key_7=value_7, key_8=value_8, key_9=value_9}";

        fillMap(map);
        assertEquals("test map.put() with predictable order...", expectedLinkedHashMapOrder, map.toString());
    }

    private static void fillMap(Map<String, String> map) {
        for (int i = 0; i < 10; i++) {
            int objectId = countObjects++;
            map.put("key_" + objectId, "value_" + objectId);
        }
    }
}
