package com.getjavajob.training.algo08.mamaeva.lesson10;

import com.getjavajob.training.algo08.util.StopWatch;
import org.junit.Ignore;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Results of comparative sort tests with size of array equals 100_000 of int:
 * test bubbleSort time... 21_238 ms
 * test insertionSort time... 2_334 ms
 * test quickSort time... 23 ms
 * test mergeSort time... 30 ms
 * test Arrays.sort() time... 61 ms
 * <p>
 * Results of comparative sort tests with size of array equals Integer.MAX_VALUE/10 of int:
 * test quickSort time... 33_553 ms / 0.8 Gb
 * test mergeSort time... 55_412 ms / (0.8 + 0.8) Gb
 * test Arrays.sort() time... 26_504 ms / 0.8 Gb
 * <p>
 * QuickSort for Integer.MAX_VALUE-2: 360_689 ms / 8 Gb
 * DualPivotQuickSort (Arrays.sort()) for Integer.MAX_VALUE-2: 286_223 ms / 8 Gb
 */

public class SortJUnitTest {
    private static StopWatch time = new StopWatch();
    private static int[] array = {3, 7, 7, 4, 1, 0, 9, 6, 2, 2, 8, 6, 5};
    private static int[] expectedArray = {0, 1, 2, 2, 3, 4, 5, 6, 6, 7, 7, 8, 9};

    @org.junit.Before
    public void setUp() throws Exception {
        array = new int[]{3, 7, 7, 4, 1, 0, 9, 6, 2, 2, 8, 6, 5};
    }

    @org.junit.Test
    public void testBubbleSortSimple() {
        Sort.bubbleSort(array);
        assertArrayEquals("test bubbleSort(array)...", expectedArray, array);
    }

    @org.junit.Test
    public void testInsertionSortSimple() {
        Sort.insertionSort(array);
        assertArrayEquals("test insertionSort(array)...", expectedArray, array);
    }

    @org.junit.Test
    public void testQuickSortSimple() {
        Sort.quickSort(array);
        assertArrayEquals("test quickSort(array)...", expectedArray, array);
    }

    @org.junit.Test
    public void testMergeSortSimple() {
        Sort.mergeSort(array);
        assertArrayEquals("test mergeSort(array)...", expectedArray, array);
    }

    @Ignore
    @org.junit.Test
    public void testQuickSortMax() {
        final int ARRAY_SIZE = Integer.MAX_VALUE - 2;
        testQuickSort(ARRAY_SIZE);
    }

    @Ignore
    @org.junit.Test
    public void testMergeSortMax() {
        final int ARRAY_SIZE = Integer.MAX_VALUE / 10;
        testMergeSort(ARRAY_SIZE);
    }

    @Ignore
    @org.junit.Test
    public void comparativeTest() {
        final int ARRAY_SIZE = 100_000;
        testBubbleSort(ARRAY_SIZE);
        testInsertionSort(ARRAY_SIZE);
        testQuickSort(ARRAY_SIZE);
        testMergeSort(ARRAY_SIZE);
        testArraysSort(ARRAY_SIZE);
    }

    private void testBubbleSort(int size) {
        System.gc();
        int[] arr = fillArray(size);
        time.start();
        Sort.bubbleSort(arr);
        System.out.println("test bubbleSort time... " + time.getElapsedTime());
        assertTrue("test bubbleSort(" + arr.length + ")...", isArraySorted(arr));
    }

    private void testInsertionSort(int size) {
        System.gc();
        int[] arr = fillArray(size);
        time.start();
        Sort.insertionSort(arr);
        System.out.println("test insertionSort time... " + time.getElapsedTime());
        assertTrue("test insertionSort(" + arr.length + ")...", isArraySorted(arr));
    }

    private void testQuickSort(int size) {
        System.gc();
        int[] arr = fillArray(size);
        time.start();
        Sort.quickSort(arr);
        System.out.println("test quickSort time... " + time.getElapsedTime());
        assertTrue("test quickSort(" + arr.length + ")...", isArraySorted(arr));
    }

    private void testMergeSort(int size) {
        System.gc();
        int[] arr = fillArray(size);
        time.start();
        Sort.mergeSort(arr);
        System.out.println("test mergeSort time... " + time.getElapsedTime());
        assertTrue("test mergeSort(" + arr.length + ")...", isArraySorted(arr));
    }

    private void testArraysSort(int size) { // JDK used DualPivotQuickSort
        System.gc();
        int[] arr = fillArray(size);
        time.start();
        Arrays.sort(arr);
        System.out.println("test Arrays.sort() time... " + time.getElapsedTime());
        assertTrue("test Arrays.sort(" + arr.length + ")...", isArraySorted(arr));
    }

    private int[] fillArray(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * Integer.MAX_VALUE);
        }
        return arr;
    }

    private boolean isArraySorted(int[] arr) { // result of test: false if arr[i-1] > arr[i]
        for (int i = 1; i < arr.length; i++) {
            if (arr[i - 1] > arr[i]) {
                return false;
            }
        }
        return true;
    }
}