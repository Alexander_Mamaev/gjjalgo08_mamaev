package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<V> extends AbstractList<V> implements List<V> {
    private int size;
    private Element<V> first;
    private Element<V> last;
    private int modCount;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean add(V val) {
        linkLast(val);
        return true;
    }

    @Override
    public void add(int index, V val) {
        checkPositionIndex(index);
        if (index == size) {
            linkLast(val);
        } else {
            linkBefore(val, element(index));
        }
    }

    @Override
    public boolean remove(Object val) {
        if (val == null) {
            for (Element<V> x = first; x != null; x = x.next) {
                if (x.val == null) {
                    unlink(x);
                    return true;
                }
            }
        } else {
            for (Element<V> x = first; x != null; x = x.next) {
                if (val.equals(x.val)) {
                    unlink(x);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public V remove(int index) {
        checkElementIndex(index);
        return unlink(element(index));
    }

    @Override
    public V get(int index) {
        checkElementIndex(index);
        return element(index).val;
    }

    @Override
    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    private void linkLast(V val) {
        Element<V> l = last;
        Element<V> e = new Element<>(l, null, val);
        last = e;
        if (l == null) {
            first = e;
        } else {
            l.next = e;
        }
        size++;
        modCount++;
    }

    private void linkBefore(V element, Element<V> shiftedElement) {
        final Element<V> prev = shiftedElement.prev;
        final Element<V> newElement = new Element<>(prev, shiftedElement, element);
        shiftedElement.prev = newElement;
        if (prev == null) {
            first = newElement;
        } else {
            prev.next = newElement;
        }
        size++;
        modCount++;
    }

    private V unlink(Element<V> x) {
        final V element = x.val;
        final Element<V> next = x.next;
        final Element<V> prev = x.prev;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.val = null;
        size--;
        modCount++;
        return element;
    }

    private Element<V> element(int index) {
        if (index < (size >> 1)) {
            Element<V> x = first;
            for (int i = 0; i < index; i++) {
                x = x.next;
            }
            return x;
        } else {
            Element<V> x = last;
            for (int i = size - 1; i > index; i--) {
                x = x.prev;
            }
            return x;
        }
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Element<V> x = first; x != null; x = x.next) {
            result[i++] = x.val;
        }
        return result;
    }

    private static class Element<V> {
        private Element<V> next;
        private Element<V> prev;
        private V val;

        Element(Element<V> prev, Element<V> next, V element) {
            this.val = element;
            this.next = next;
            this.prev = prev;
        }
    }

    public class ListIteratorImpl implements ListIterator<V> {
        private Element<V> lastReturned = null;
        private Element<V> next;
        private int nextIndex;
        private int expectedModCount = modCount;

        public ListIteratorImpl(int index) {
            next = (index == size) ? null : element(index);
            nextIndex = index;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size;
        }

        @Override
        public V next() {
            checkForComodification();
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.val;
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        @Override
        public V previous() {
            checkForComodification();
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastReturned = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.val;
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            checkForComodification();
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            Element<V> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned) {
                next = lastNext;
            } else {
                nextIndex--;
            }
            lastReturned = null;
            expectedModCount++;
        }

        @Override
        public void set(V e) {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            checkForComodification();
            lastReturned.val = e;
        }

        @Override
        public void add(V e) {
            checkForComodification();
            lastReturned = null;
            if (next == null) {
                linkLast(e);
            } else {
                linkBefore(e, next);
            }
            nextIndex++;
            expectedModCount++;
        }

        private void checkForComodification() {
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
