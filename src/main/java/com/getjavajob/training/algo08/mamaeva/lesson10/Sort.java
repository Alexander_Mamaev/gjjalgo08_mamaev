package com.getjavajob.training.algo08.mamaeva.lesson10;

public class Sort {
    public static void bubbleSort(int[] array) {
        int size = array.length;
        for (int i = 0; i < size; i++) {
            for (int j = 1; j < size - i; j++) {
                if (array[j - 1] > array[j]) {
                    swap(array, j, j - 1);
                }
            }
        }
    }

    public static void insertionSort(int[] array) {
        int size = array.length;
        for (int i = 0; i < size; i++) {
            for (int j = i; j > 0 && array[j - 1] > array[j]; j--) {
                swap(array, j, j - 1);
            }
        }
    }

    public static void quickSort(int[] array) {
        quickSort(array, 0, array.length - 1);
    }

    private static void quickSort(int[] array, int left, int right) {
        int l = left;
        int r = right;
        int pivot = array[left - (left - right) / 2];
        while (l <= r) {
            while (array[l] < pivot) {
                l++;
            }
            while (array[r] > pivot) {
                r--;
            }
            if (l <= r) {
                swap(array, l, r);
                l++;
                r--;
            }
        }
        if (left < r) {
            quickSort(array, left, r);
        }
        if (l < right) {
            quickSort(array, l, right);
        }
    }

    private static void swap(int[] array, int x, int y) {
        int temp;
        temp = array[x];
        array[x] = array[y];
        array[y] = temp;
    }

    public static void mergeSort(int[] array) {
        int[] tmp = new int[array.length];
        mergeSort(array, tmp, 0, array.length);
    }

    private static void mergeSort(int[] arr, int[] tmp, int low, int high) {
        int size = high - low;
        if (size > 1) {
            int middle = low + (size >>> 1);
            mergeSort(arr, tmp, low, middle);
            mergeSort(arr, tmp, middle, high);

            int l = low;
            int m = middle;
            for (int i = 0; i < size; i++) {
                if (m >= high || l < middle && arr[l] < arr[m]) {
                    tmp[i] = arr[l++];
                } else {
                    tmp[i] = arr[m++];
                }
            }
            System.arraycopy(tmp, 0, arr, low, size);
        }
    }
}

