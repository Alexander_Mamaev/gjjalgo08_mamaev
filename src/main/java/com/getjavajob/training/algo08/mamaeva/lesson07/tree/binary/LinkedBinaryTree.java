package com.getjavajob.training.algo08.mamaeva.lesson07.tree.binary;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    protected NodeImpl<E> root;
    protected int size;

    // nonpublic utility

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> validateNode;
        try {
            validateNode = (NodeImpl<E>) n;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("type of the node is not valid");
        }
        return validateNode;
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (size != 0) {
            throw new IllegalStateException("the root is already exist");
        }
        root = new NodeImpl<>(e);
        size++;
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        Node<E> returnedNode;
        if (node.left == null) {
            returnedNode = addLeft(node, e);
        } else if (node.right == null) {
            returnedNode = addRight(node, e);
        } else {
            throw new IllegalArgumentException("the node has left and right child already");
        }
        return returnedNode;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.left != null) {
            throw new IllegalArgumentException("the left child is already exist");
        }
        NodeImpl<E> returnedNode = new NodeImpl<>(e);
        returnedNode.parent = node;
        node.left = returnedNode;
        size++;
        return returnedNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.right != null) {
            throw new IllegalArgumentException("the right child is already exist");
        }
        NodeImpl<E> returnedNode = new NodeImpl<>(e);
        returnedNode.parent = node;
        node.right = returnedNode;
        size++;
        return returnedNode;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E oldElement = node.getElement();
        node.element = e;
        return oldElement;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E element = node.element;
        removeChildesOf(node);
        if (node == root) {
            root = null;
        } else {
            if (node == node.parent.left) {
                node.parent.left = null;
            } else {
                node.parent.right = null;
            }
        }
        size--;
        return element;
    }

    private void removeChildesOf(Node<E> n) {
        NodeImpl<E> node = validate(n);
        if (node.left != null) {
            removeChildesOf(node.left);
        }
        if (node.right != null) {
            removeChildesOf(node.right);
        }
        if (node.parent.left != null) {
            node.parent.left = null;
            size--;
        }
        if (node.parent.right != null) {
            node.parent.right = null;
            size--;
        }
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return node == null ? null : node.left;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return node == null ? null : node.right;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        return node == null ? null : node.parent;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return super.iterator();
    }

    @Override
    public Iterable<Node<E>> nodes() {
        return preOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        public E element;
        public NodeImpl<E> parent;
        public NodeImpl<E> left;
        public NodeImpl<E> right;

        public NodeImpl(E element) {
            this.element = element;
        }

        @Override
        public E getElement() {
            return element;
        }

        @Override
        public String toString() {
            return "NodeImpl{" +
                    "element=" + element +
                    '}';
        }
    }
}