package com.getjavajob.training.algo08.mamaeva.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

public class CollectionUtils {

    public static <E> Collection<E> unmodifiableCollection(Collection<E> collection) {
        return Collections.unmodifiableCollection(collection);
    }

    /**
     * Executes the given closure on each element in the collection.
     */
    public static <E> void forAllDo(final Iterable<E> iterable, final Closure<? super E> closure) {
        Objects.requireNonNull(closure, "Closure must not be null");
        for (E element : iterable) {
            closure.execute(element);
        }
    }

    /**
     * Filter the collection by applying a Predicate to each element. If the
     * predicate returns false, remove the element.
     */
    public static <T> Collection<T> filter(Collection<T> target, Predicate<T> predicate) {
        Collection<T> result = new ArrayList<>();
        for (T element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    /**
     * Returns a new Collection containing all elements of the input collection transformed by the given transformer.
     */
    public static <I, O> Collection<O> getTransform(Iterable<I> inputCollection, Transformer<? super I, ? extends O> transformer) {
        Collection<O> outputCollection = new ArrayList<>();
        for (I element : inputCollection) {
            outputCollection.add(transformer.transform(element));
        }
        return outputCollection;
    }

    /**
     * Transform the collection by applying a Transformer to each element.
     * Implemented with raw type conversion.
     */
    @SuppressWarnings("unchecked")
    public static Collection transform(Collection collection, Transformer transformer) {
        Collection newCollection = new ArrayList(collection);
        collection.clear();
        collection.addAll(getTransform(newCollection, transformer));
        return collection;
    }

    /**
     * Defines a functor interface implemented by classes that perform a predicate
     * test on an object.
     */
    public interface Predicate<T> {
        boolean apply(T element);
    }

    /**
     * Defines a functor interface implemented by classes that transform one
     * object into another.
     */
    public interface Transformer<I, O> {
        O transform(I input);
    }

    /**
     * Defines a functor interface implemented by classes that do something.
     */
    public interface Closure<T> {
        void execute(T input);
    }
}
