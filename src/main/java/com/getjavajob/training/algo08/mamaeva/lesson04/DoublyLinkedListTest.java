package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class DoublyLinkedListTest {
    private static int countNewObjects;

    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        DoublyLinkedList<String> dLL = new DoublyLinkedList<>();

        testAddEnd("Test dLL.add(toEnd)...", list, dLL);
        testAddMiddle("Test dLL.add(toMiddle)...", list, dLL);
        testAddBegin("Test dLL.add(toBegin)...", list, dLL);
        testRemoveEnd("Test dLL.removeHead(fromEnd)...", list, dLL);
        testRemoveMiddle("Test dLL.removeHead(fromMiddle)...", list, dLL);
        testRemoveBegin("Test dLL.removeHead(fromBegin)...", list, dLL);
        testRemoveElement("Test dLL.removeHead(Element)...", list, dLL);
        testGetEnd("Test dll.get(fromEnd)...", list, dLL);
        testGetMiddle("Test dLL.get(fromMiddle)...", list, dLL);
        testGetBegin("Test dLL.get(fromBegin)...", list, dLL);

        printTestResult();
    }

    private static void testGetBegin(String msg, List<String> list, List<String> dLL) {
        int index = 0;
        assertEquals(msg, list.get(index), dLL.get(index));
    }

    private static void testGetMiddle(String msg, List<String> list, List<String> dLL) {
        int index = dLL.size() >> 1;
        assertEquals(msg, list.get(index), dLL.get(index));
    }

    private static void testGetEnd(String msg, List<String> list, List<String> dLL) {
        int index = dLL.size() - 1;
        assertEquals(msg, list.get(index), dLL.get(index));
    }

    private static void testRemoveElement(String msg, List<String> list, List<String> dLL) {
        String element = "Object3";
        dLL.remove(element);
        list.remove(element);
        assertEquals(msg, list, dLL);
    }

    private static void testRemoveBegin(String msg, List<String> list, List<String> dLL) {
        int index = 0;
        dLL.remove(index);
        list.remove(index);
        assertEquals(msg, list, dLL);
    }

    private static void testRemoveMiddle(String msg, List<String> list, List<String> dLL) {
        int index = dLL.size() >> 1;
        dLL.remove(index);
        list.remove(index);
        assertEquals(msg, list, dLL);
    }

    private static void testRemoveEnd(String msg, List<String> list, List<String> dLL) {
        int index = dLL.size() - 1;
        dLL.remove(index);
        list.remove(index);
        assertEquals(msg, list, dLL);
    }

    private static void testAddBegin(String msg, List<String> list, List<String> dLL) {
        int index = 0;
        String newObject = getNewObject();
        dLL.add(index, newObject);
        list.add(index, newObject);
        assertEquals(msg, list, dLL);
    }

    private static void testAddMiddle(String msg, List<String> list, List<String> dLL) {
        int index = dLL.size() >> 1;
        String newObject = getNewObject();
        dLL.add(index, newObject);
        list.add(index, newObject);
        assertEquals(msg, list, dLL);
    }

    private static void testAddEnd(String msg, List<String> list, List<String> dLL) {
        for (int i = 0; i < 7; i++) {
            list.add("Object" + i);
            dLL.add("Object" + i);
        }
        assertEquals(msg, list, dLL);
    }

    private static String getNewObject() {
        return "newObject" + countNewObjects++;
    }
}
