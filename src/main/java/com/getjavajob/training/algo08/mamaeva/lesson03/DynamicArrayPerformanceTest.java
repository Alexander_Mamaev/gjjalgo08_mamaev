package com.getjavajob.training.algo08.mamaeva.lesson03;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.ArrayList;
import java.util.List;

public class DynamicArrayPerformanceTest {
    public static void main(String[] args) throws InterruptedException {
        StopWatch time = new StopWatch();
        DynamicArray dynamicArray;
        List<Object> list;

        dynamicArray = timeTestDaAddBegin(time, new DynamicArray(), 200_000_000);
        timeTestDaRemovedBegin(time, dynamicArray, dynamicArray.size() / 10_000_000);
        System.gc();
        list = timeTestListAddBegin(time, new ArrayList<>(), 200_000_000);
        timeTestListRemovedBegin(time, list, list.size() / 10_000_000);
        System.gc();
        dynamicArray = timeTestDaAddMiddle(time, new DynamicArray(), 200_000);
        timeTestDaRemoveMiddle(time, dynamicArray, dynamicArray.size());
        System.gc();
        list = timeTestListAddMiddle(time, new ArrayList<>(), 200_000);
        timeTestListRemovedMiddle(time, list, list.size());
        System.gc();
        dynamicArray = timeTestDaAddEnd(time, new DynamicArray(), 200_000_000);
        timeTestDaRemovedEnd(time, dynamicArray, dynamicArray.size());
        System.gc();
        list = timeTestListAddEnd(time, new ArrayList<>(), 200_000_000);
        timeTestListRemoveEnd(time, list, list.size());
    }

    private static DynamicArray timeTestDaAddBegin(StopWatch time, DynamicArray dynamicArray, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dynamicArray.add("");
        }
        System.out.println("DynamicArray.add(toBegin): " + time.getElapsedTime() + " ms");
        return dynamicArray;
    }

    private static void timeTestDaRemovedBegin(StopWatch time, DynamicArray dynamicArray, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dynamicArray.remove(0);
        }
        System.out.println("DynamicArray.removed(fromBegin): " + time.getElapsedTime() + " ms");
    }

    private static List<Object> timeTestListAddBegin(StopWatch time, List<Object> list, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.add("");
        }
        System.out.println("ArrayList.add(toBegin): " + time.getElapsedTime() + " ms");
        return list;
    }

    private static void timeTestListRemovedBegin(StopWatch time, List<Object> list, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.remove(0);
        }
        System.out.println("ArrayList.removeHead(fromBegin): " + time.getElapsedTime() + " ms");
    }

    private static DynamicArray timeTestDaAddMiddle(StopWatch time, DynamicArray dynamicArray, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dynamicArray.add(dynamicArray.size() / 2, "");
        }
        System.out.println("DynamicArray.add(toMiddle): " + time.getElapsedTime() + " ms");
        return dynamicArray;
    }

    private static void timeTestDaRemoveMiddle(StopWatch time, DynamicArray dynamicArray, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dynamicArray.remove(dynamicArray.size() / 2);
        }
        System.out.println("DynamicArray.removeHead(fromMiddle): " + time.getElapsedTime() + " ms");
    }

    private static List<Object> timeTestListAddMiddle(StopWatch time, List<Object> list, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.add(list.size() / 2, "");
        }
        System.out.println("ArrayList.add(toMiddle): " + time.getElapsedTime() + " ms");
        return list;
    }

    private static void timeTestListRemovedMiddle(StopWatch time, List<Object> list, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.remove(list.size() / 2);
        }
        System.out.println("ArrayList.removeHead(fromMiddle): " + time.getElapsedTime() + " ms");
    }

    private static DynamicArray timeTestDaAddEnd(StopWatch time, DynamicArray dynamicArray, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dynamicArray.add(dynamicArray.size(), "");
        }
        System.out.println("DynamicArray.add(toEnd): " + time.getElapsedTime() + " ms");
        return dynamicArray;
    }

    private static void timeTestDaRemovedEnd(StopWatch time, DynamicArray dynamicArray, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            dynamicArray.remove(dynamicArray.size() - 1);
        }
        System.out.println("DynamicArray.removeHead(fromEnd): " + time.getElapsedTime() + " ms");
    }

    private static List<Object> timeTestListAddEnd(StopWatch time, List<Object> list, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.add(list.size(), "");
        }
        System.out.println("ArrayList.add(toEnd): " + time.getElapsedTime() + " ms");
        return list;
    }

    private static void timeTestListRemoveEnd(StopWatch time, List<Object> list, int size) {
        time.start();
        for (int i = 0; i < size; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("ArrayList.removeHead(fromEnd): " + time.getElapsedTime() + " ms");
    }
}
