package com.getjavajob.training.algo08.mamaeva.lesson04;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SinglyLinkedListTest {
    private static SinglyLinkedList<Object> list = new SinglyLinkedList<>();
    private static List<Object> linkedList = new LinkedList<>();

    public static void main(String[] args) {
        testAdd();
        testReverse();
        performanceTestRevers();
    }

    private static void performanceTestRevers() {
        long time10k = testReverseTime(1_000_000);
        System.out.println(time10k + " " + time10k / 1.);
        long time20k = testReverseTime(2_000_000);
        System.out.println(time20k + " " + time20k / 2.);
        long time40k = testReverseTime(4_000_000);
        System.out.println(time40k + " " + time40k / 4.);
        long time80k = testReverseTime(8_000_000);
        System.out.println(time80k + " " + time80k / 8.);
        double rate = (time80k / 8. + time40k / 4. + time20k / 2.) / (3. * time10k);
        System.out.printf("revers time = %.1f * n", rate);

    }

    private static long testReverseTime(int size) {
        list = new SinglyLinkedList<>();
        fillList(size);
        StopWatch time = new StopWatch();
        time.start();
        list.reverse();
        return time.getElapsedTime();
    }

    private static void fillList(int size) {
        for (int i = 0; i < size; i++) {
            list.add("Object" + i);
        }
    }

    private static void testReverse() {
        list.reverse();
        Collections.reverse(linkedList);
        assertEquals("test list.reverse()...", linkedList.toArray(), list.toArray());
    }

    private static void testAdd() {
        fillLists(10);
        assertEquals("test add...", linkedList.toArray(), list.toArray());
    }

    private static void fillLists(int size) {
        for (int i = 0; i < size; i++) {
            String string = "Object" + i;
            list.add(string);
            linkedList.add(string);
        }
    }

    private static void printList(SinglyLinkedList<Object> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
