package com.getjavajob.training.algo08.mamaeva.lesson09;

import java.util.NoSuchElementException;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.util.Assert.*;

public class SortedSetTest {
    private static String expected;

    public static void main(String[] args) {
        SortedSet<Integer> emptySet = new TreeSet<>();
        SortedSet<Integer> set = new TreeSet<>();
        set.add(5);
        set.add(6);
        set.add(4);
        set.add(7);
        set.add(3);
        set.add(8);
        set.add(2);
        set.add(9);
        set.add(1);

        subStetTest(set);
        headSetTest(set);
        tailSetTest(set);
        firstTest(emptySet, set);
        lastTest(emptySet, set);

        printTestResult();

    }

    private static void lastTest(SortedSet<Integer> emptySet, SortedSet<Integer> set) {
        assertEquals("test set.last()...", 9, (int) set.last());
        try {
            emptySet.last();
            fail("expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test emptySet.last()... expected NoSuchElementException", NoSuchElementException.class, e.getClass());
        }
    }

    private static void firstTest(SortedSet<Integer> emptySet, SortedSet<Integer> set) {
        assertEquals("test set.first()...", 1, (int) set.first());
        try {
            emptySet.first();
            fail("expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test emptySet.first()... expected NoSuchElementException", NoSuchElementException.class, e.getClass());
        }
    }

    private static void tailSetTest(SortedSet<Integer> set) {
        expected = "[5, 6, 7, 8, 9]";
        assertEquals("test tailSet(5)...", expected, set.tailSet(5).toString());
        try {
            set.tailSet(null);
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test tailSet(null)... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void headSetTest(SortedSet<Integer> set) {
        expected = "[1, 2, 3, 4]";
        assertEquals("test headSet(5)...", expected, set.headSet(5).toString());
        try {
            set.headSet(null);
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test headSet(null)... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void subStetTest(SortedSet<Integer> set) {
        expected = "[3, 4, 5, 6, 7]";
        assertEquals("test subSet(3, 8)...", expected, set.subSet(3, 8).toString());
        try {
            set.subSet(null, null);
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test subSet(null, null)... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }
}
