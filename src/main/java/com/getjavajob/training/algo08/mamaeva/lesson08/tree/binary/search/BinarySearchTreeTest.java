package com.getjavajob.training.algo08.mamaeva.lesson08.tree.binary.search;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import java.util.Comparator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class BinarySearchTreeTest {
    public static void main(String[] args) {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        Node<Integer> root = bst.add(8);
        Node<Integer> n3 = bst.add(3);
        Node<Integer> n1 = bst.add(1);
        Node<Integer> n6 = bst.add(6);
        Node<Integer> n4 = bst.add(4);
        Node<Integer> n7 = bst.add(7);
        Node<Integer> n10 = bst.add(10);
        Node<Integer> n14 = bst.add(14);
        Node<Integer> n13 = bst.add(13);

        assertEquals("test treeSearch(Node, n)...", n14, bst.treeSearch(root, 14));
        assertEquals("test treeSearch(Node, n)...", n4, bst.treeSearch(root, 4));
        assertEquals("test treeSearch(Node, n)...", n4, bst.treeSearch(n3, 4));

        BinarySearchTree<String> bstS = new BinarySearchTree<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                int result = o1.length() - o2.length();
                if (result > 0) {
                    return 1;
                } else if (result < 0) {
                    return -1;
                }
                return 0;
            }
        });
        Node<String> rootS = bstS.add("8_______");
        Node<String> n3S = bstS.add("3__");
        Node<String> n1S = bstS.add("1");
        Node<String> n6S = bstS.add("6_____");
        Node<String> n4S = bstS.add("4___");
        Node<String> n7S = bstS.add("7______");
        Node<String> n10S = bstS.add("10________");
        Node<String> n14S = bstS.add("14____________");
        Node<String> n13S = bstS.add("13___________");

        assertEquals("test add() with comparator...", "(8_______(3__(1, 6_____(4___, 7______)), 10________(_, 14____________(13___________, _))))", bstS.toString());
        assertEquals("test <String> treeSearch(Node, n)...", n14S, bstS.treeSearch(rootS, "14____________"));
        assertEquals("test <String> treeSearch(Node, n)...", n4S, bstS.treeSearch(rootS, "4___"));
        assertEquals("test <String> treeSearch(Node, n)...", n4S, bstS.treeSearch(n3S, "4___"));
        assertEquals("test <String> treeSearch(Node, n) null expected...", null, bstS.treeSearch(rootS, ""));

        BinarySearchTree<Integer> bstI = new BinarySearchTree<>();
        bstI.add(100);
        bstI.add(90);
        bstI.add(110);
        bstI.add(60);
        bstI.add(30);
        bstI.add(20);
        bstI.add(10);
        bstI.add(25);
        bstI.add(40);
        bstI.add(35);
        bstI.add(45);
        bstI.add(70);
        bstI.add(50);
        bstI.add(47);
        bstI.add(80);
        bstI.add(78);
        bstI.add(82);
        bstI.add(65);
        bstI.add(63);
        bstI.add(67);
        bstI.add(150);
        bstI.add(160);
        bstI.add(155);
        bstI.add(158);
        bstI.add(170);

        String expected = "(100(90(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), 80(78, 82))), _), 110(_, 150(_, 160(155(_, 158), 170)))))";
        assertEquals("test add(E)...", expected, bstI.toString());

        bstI.remove(90);
        expected = "(100(82(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), 80(78, _))), _), 110(_, 150(_, 160(155(_, 158), 170)))))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(82);
        expected = "(100(80(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), 78)), _), 110(_, 150(_, 160(155(_, 158), 170)))))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(80);
        expected = "(100(78(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 110(_, 150(_, 160(155(_, 158), 170)))))";
        assertEquals("test remove(E)...", expected, bstI.toString());

        bstI.remove(160);
        expected = "(100(78(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 110(_, 150(_, 158(155, 170)))))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(110);
        expected = "(100(78(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 150(_, 158(155, 170))))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(150);
        expected = "(100(78(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 155(_, 158(_, 170))))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(170);
        expected = "(100(78(60(30(20(10, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 155(_, 158)))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(10);
        expected = "(100(78(60(30(20(_, 25), 40(35, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 155(_, 158)))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(25);
        expected = "(100(78(60(30(20, 40(35, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 155(_, 158)))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(40);
        expected = "(100(78(60(30(20, 35(_, 45(_, 50(47, _)))), 70(65(63, 67), _)), _), 155(_, 158)))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(60);
        expected = "(100(78(50(30(20, 35(_, 45(_, 47))), 70(65(63, 67), _)), _), 155(_, 158)))";
        assertEquals("test remove(E)...", expected, bstI.toString());
        bstI.remove(100);
        expected = "(78(50(30(20, 35(_, 45(_, 47))), 70(65(63, 67), _)), 155(_, 158)))";
        assertEquals("test remove(E)...", expected, bstI.toString());

        for (Integer tree : bstI) {
            System.out.println(bstI);
            bstI.remove(bstI.root());
        }
        assertEquals("test size of removed tree after all element deletion...", 0, bstI.size());

        printTestResult();
    }
}
