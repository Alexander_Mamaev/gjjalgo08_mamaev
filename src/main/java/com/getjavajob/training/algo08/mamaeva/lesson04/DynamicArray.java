package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.*;

public class DynamicArray<V> extends AbstractList<V> implements List<V> {

    private Object[] dynamicArray;
    private int size;
    private int modCount = 0;

    public DynamicArray() {
        dynamicArray = new Object[10];
    }

    public DynamicArray(int length) {
        dynamicArray = new Object[length];
    }

    /**
     * adds element to the end. returns whether element was added.
     */
    public boolean add(V e) {
        growFullArray();
        dynamicArray[size++] = e;
        modCount++;
        return true;
    }

    /**
     * shifts other elements right after insertion
     */
    public void add(int i, V e) {
        if (i > size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(i));
        } else if (i == size) {
            add(e);
        } else {
            growFullArray();
            System.arraycopy(dynamicArray, i, dynamicArray, i + 1, size - i);
            dynamicArray[i] = e;
            size++;
            modCount++;
        }
    }

    /**
     * @return prev element
     */
    public V set(int i, Object e) {
        rangeCheck(i);
        V oldValue = dynamicArray(i);
        dynamicArray[i] = e;
        modCount++;
        return oldValue;
    }

    @SuppressWarnings("unchecked")
    private V dynamicArray(int i) {
        return (V) dynamicArray[i];
    }

    public V get(int i) {
        rangeCheck(i);
        return dynamicArray(i);
    }

    public V remove(int i) {
        rangeCheck(i);
        V objRemoved = dynamicArray(i);
        int numMoved = size - i - 1;
        if (numMoved > 0) {
            System.arraycopy(dynamicArray, i + 1, dynamicArray, i, numMoved);
        }
        dynamicArray[--size] = null;
        modCount++;
        return objRemoved;
    }

    /**
     * removes first occurrence and returns true if it was removed
     */
    public boolean remove(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (dynamicArray[i] == null) {
                    fastRemove(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (e.equals(dynamicArray[i])) {
                    fastRemove(i);
                    return true;
                }
            }
        }
        return false;
    }

    private void fastRemove(int i) {
        int numMoved = size - i - 1;
        if (numMoved > 0) {
            System.arraycopy(dynamicArray, i + 1, dynamicArray, i,
                    numMoved);
        }
        dynamicArray[--size] = null;
        modCount++;
    }

    public int size() {
        return size;
    }

    public int indexOf(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (dynamicArray[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (e.equals(dynamicArray[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }

    public Object[] toArray() {
        return Arrays.copyOf(dynamicArray, size);
    }

    /**
     * grow in 1.5 times if it is full.
     */
    private void growFullArray() {
        if (dynamicArray.length == size) {
            Object[] newDynamicArray = new Object[(int) (dynamicArray.length * 1.5)];
            System.arraycopy(dynamicArray, 0, newDynamicArray, 0, dynamicArray.length);
            dynamicArray = newDynamicArray;
        }
    }

    private void rangeCheck(int i) {
        if (i >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(i));
        }
    }

    private String outOfBoundsMsg(int i) {
        return "Index: " + i + ", Size: " + size;
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    /**
     * ListIteratorImpl -----------------------------------------------------
     */
    public class ListIteratorImpl implements ListIterator<V> {
        private int cursor; // index of next element to return
        private int lastRet = -1; // index of tail returned element; -1 if no such
        private int expectedModCount = modCount;

        public boolean hasNext() {
            return cursor != size;
        }

        @SuppressWarnings("unchecked")
        public V next() {
            checkForComodification();
            int i = cursor;
            if (i >= size) {
                throw new NoSuchElementException();
            }
            Object[] elementData = dynamicArray;
            if (i >= elementData.length) {
                throw new ConcurrentModificationException();
            }
            cursor = i + 1;
            return (V) elementData[lastRet = i];
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        @SuppressWarnings("unchecked")
        public V previous() {
            checkForComodification();
            int i = cursor - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            }
            Object[] elementData = dynamicArray;
            if (i >= elementData.length) {
                throw new ConcurrentModificationException();
            }
            cursor = i;
            return (V) elementData[lastRet = cursor];
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            if (lastRet < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();

            try {
                fastRemove(lastRet);
                cursor = lastRet;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void set(V e) {
            if (lastRet < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();

            try {
                DynamicArray.this.set(lastRet, e);
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(V e) {
            checkForComodification();

            try {
                int i = cursor;
                DynamicArray.this.add(i, e);
                cursor = i + 1;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        private void checkForComodification() {
            if (expectedModCount != DynamicArray.this.modCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
