package com.getjavajob.training.algo08.mamaeva.lesson05;

import com.getjavajob.training.algo08.mamaeva.lesson04.SinglyLinkedList;

public class LinkedListQueue<E> extends AbstractLinkedListQueue<E> {
    private SinglyLinkedList<E> list = new SinglyLinkedList<>();

    @Override
    public boolean add(E e) {
        list.add(e);
        return true;
    }

    @Override
    public E remove() {
        return list.removeHead();
    }

    @Override
    public int size() {
        return list.size();
    }
}

