package com.getjavajob.training.algo08.mamaeva.lesson06;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.HashMap;
import java.util.Map;

public class AssociativeArrayVsMapPerformanceTest {
    private static StopWatch time = new StopWatch();
    private static AssociativeArray<String, String> aa = new AssociativeArray<>();
    private static Map<String, String> map = new HashMap<>();

    public static void main(String[] args) { // VM Options: -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xms6G
        long mapResult = 0;
        long aaResult = 0;
        for (int i = 0; i < 10; i++) {
            mapResult = +mapTest();
            aaResult = +aaTest();
        }
        System.out.println("map result: " + mapResult / 10);
        System.out.println("aa result: " + aaResult / 10);
    }

    private static long aaTest() {
        System.gc();
        System.out.println("-------------------Start aaTest---------------------");
        time.start();
        for (int i = 0; i < 1_000; i++) {
            aa.add("polygenelubricants", "Value_polygenelubricants");
            aa.add("GydZG_", "Value_GydZG_");
            aa.add("DESIGNING WORKHOUSES", "Value_DSIGNING WORKHOUSES");
            for (int j = 0; j < 10_000; j++) {
                aa.add("key_" + j, "value_" + j);
            }
        }
        System.out.println("-------------------Finished aaTest---------------------");
        return time.getElapsedTime();
    }

    private static long mapTest() {
        System.gc();
        System.out.println("-------------------Start mapTest---------------------");
        time.start();
        for (int i = 0; i < 1_000; i++) {
            map.put("polygenelubricants", "Value_polygenelubricants");
            map.put("GydZG_", "Value_GydZG_");
            map.put("DESIGNING WORKHOUSES", "Value_DSIGNING WORKHOUSES");
            for (int j = 0; j < 10_000; j++) {
                map.put("key_" + j, "value_" + j);
            }
        }
        System.out.println("-------------------Finished mapTest---------------------");
        return time.getElapsedTime();
    }
}
