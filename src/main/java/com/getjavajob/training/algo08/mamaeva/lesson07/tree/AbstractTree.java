package com.getjavajob.training.algo08.mamaeva.lesson07.tree;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return n != null && n == root();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Iterable<Node<E>> preOrder() {
        Collection<Node<E>> coll = new ArrayList<>();
        preOrder(root(), coll);
        return coll;
    }

    private void preOrder(Node<E> n, Collection<Node<E>> coll) {
        coll.add(n);
        for (Node<E> tmp : children(n)) {
            if (tmp != null) {
                preOrder(tmp, coll);
            }
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Iterable<Node<E>> postOrder() {
        Collection<Node<E>> coll = new ArrayList<>();
        postOrder(root(), coll);
        return coll;
    }

    private void postOrder(Node<E> n, Collection<Node<E>> coll) {
        for (Node<E> tmp : children(n)) {
            if (tmp != null) {
                postOrder(tmp, coll);
            }
        }
        coll.add(n);
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Iterable<Node<E>> breadthFirst() {
        Collection<Node<E>> coll = new ArrayList<>();
        Queue<Node<E>> queue = new ArrayDeque<>();
        queue.add(root());
        while (!queue.isEmpty()) {
            Node<E> n = queue.poll();
            coll.add(n);
            for (Node<E> tmp : children(n)) {
                if (tmp != null) {
                    queue.add(tmp);
                }
            }
        }
        return coll;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
