package com.getjavajob.training.algo08.mamaeva.lesson09;

import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class SortedMapTest {
    public static void main(String[] args) {
        SortedMap<Integer, Integer> smap = new TreeMap<>();
        SortedMap<Integer, Integer> empty = new TreeMap<>();

        smap.put(5, 5);
        smap.put(6, 6);
        smap.put(4, 4);
        smap.put(7, 7);
        smap.put(3, 3);
        smap.put(8, 8);
        smap.put(2, 2);
        smap.put(9, 9);
        smap.put(1, 1);

        assertEquals("test comparator()", null, smap.comparator());
        subMapTest(smap);
        headMapTest(smap);
        tailMapTest(smap);
        firstKeyTest(smap, empty);
        lastKeyTest(smap, empty);
        assertEquals("test keySet()...", "[1, 2, 3, 4, 5, 6, 7, 8, 9]", smap.keySet().toString());
        assertEquals("test values()...", "[1, 2, 3, 4, 5, 6, 7, 8, 9]", smap.values().toString());
        assertEquals("test entrySet()...", "[1=1, 2=2, 3=3, 4=4, 5=5, 6=6, 7=7, 8=8, 9=9]", smap.entrySet().toString());
    }

    private static void lastKeyTest(SortedMap<Integer, Integer> smap, SortedMap<Integer, Integer> empty) {
        assertEquals("test lastKey()...", "9", smap.lastKey().toString());
        try {
            System.out.println(empty.lastKey());
            fail("expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test tailMap()... expected NoSuchElementException", NoSuchElementException.class, e.getClass());
        }
    }

    private static void firstKeyTest(SortedMap<Integer, Integer> smap, SortedMap<Integer, Integer> empty) {
        assertEquals("test firstKey()...", "1", smap.firstKey().toString());
        try {
            System.out.println(empty.firstKey());
            fail("expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test tailMap()... expected NoSuchElementException", NoSuchElementException.class, e.getClass());
        }
    }

    private static void tailMapTest(SortedMap<Integer, Integer> smap) {
        assertEquals("test tailMap()...", "{5=5, 6=6, 7=7, 8=8, 9=9}", smap.tailMap(5).toString());
        try {
            System.out.println(smap.tailMap(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test tailMap()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void headMapTest(SortedMap<Integer, Integer> smap) {
        assertEquals("test headMap()...", "{1=1, 2=2, 3=3, 4=4, 5=5}", smap.headMap(6).toString());
        try {
            System.out.println(smap.headMap(null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test subMap()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }

    private static void subMapTest(SortedMap<Integer, Integer> smap) {
        assertEquals("test subMap()", "{4=4, 5=5, 6=6, 7=7}", smap.subMap(4, 8).toString());
        try {
            System.out.println(smap.subMap(null, null));
            fail("expected NullPointerException");
        } catch (Exception e) {
            assertEquals("test subMap()... expected NullPointerException", NullPointerException.class, e.getClass());
        }
    }
}
