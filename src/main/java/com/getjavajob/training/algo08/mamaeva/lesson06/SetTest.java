package com.getjavajob.training.algo08.mamaeva.lesson06;

import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class SetTest {
    private static int countObjects;

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        Set<String> set1 = new HashSet<>();

        fillSet(set);
        set1.addAll(set);
        assertEquals("test set.addAll(Coection)...", set.toArray(), set1.toArray());
        assertEquals("test set.add() with duplicates...", false, set1.add("Object_4"));
        String newObject = "newObject";
        set.add(newObject);
        assertEquals("test set.add()...", true, set1.add(newObject));
        assertEquals("test set.addAll() with duplicates...", false, set1.addAll(set));
        assertEquals("test set.addAll() with duplicates...", set.toArray(), set1.toArray());

        printTestResult();
    }

    private static void fillSet(Set<String> set) {
        for (int i = 0; i < 10; i++) {
            set.add("Object_" + countObjects++);
        }
    }
}
