package com.getjavajob.training.algo08.mamaeva.lesson05;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.util.Assert.*;

public class LinkedListQueueTest {
    private static final LinkedListQueue<Object> queue = new LinkedListQueue<>();
    private static final List<Object> list = new LinkedList<>();
    private static int countObjects;

    public static void main(String[] args) {
        fillQueue();
        testQueue();
        testRemoveWithException();
        testCallUnsupportedOperation();

        printTestResult();
    }

    private static void testCallUnsupportedOperation() {
        try {
            queue.clear();
            fail("...missed expected UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("Test queue.clear() call exception...", UnsupportedOperationException.class, e.getClass());
        }
    }

    private static void testQueue() {
        int i = 0;
        while (queue.size() > 0) {
            assertEquals("test queue.add() queue.removeHead()...", list.get(i++), queue.remove());
        }
    }

    private static void fillQueue() {
        for (int i = 0; i < 5; i++) {
            Object obj = getNewObject();
            queue.add(obj);
            list.add(obj);
        }
    }

    private static void testRemoveWithException() {
        try {
            queue.remove();
            fail("testRemoveWithException...missed expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("Test queue.removeHead() call exception...", NoSuchElementException.class, e.getClass());
        }
    }

    private static String getNewObject() {
        return "Object" + countObjects++;
    }
}
