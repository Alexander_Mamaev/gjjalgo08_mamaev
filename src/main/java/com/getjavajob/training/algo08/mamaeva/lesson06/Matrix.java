package com.getjavajob.training.algo08.mamaeva.lesson06;

public interface Matrix<V> {
    V get(int i, int j);

    void set(int i, int j, V value);
}
