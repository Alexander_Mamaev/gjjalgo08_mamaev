package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class ListTest {
    private static int countNewObjects;

    public static void main(String[] args) {
        List<Object> list = getList();
        List<Object> list2 = getList();

        assertEquals("test list.addAll(index, collection)", true, list.addAll(2, list2));
        assertEquals("test after list.addAll(index, collection)", getList3(), list);
        assertEquals("test list.get(2)...", "Object3", list.get(2));
        assertEquals("test list.get(0)...", "Object0", list.get(0));
        assertEquals("test list.get(5)...", "Object2", list.get(5));
        assertEquals("test list.set(2)...", "Object3", list.set(2, getNewObject()));
        assertEquals("test list.set(5)...", "Object2", list.set(5, getNewObject()));
        assertEquals("test list.set(0)...", "Object0", list.set(0, getNewObject()));
        assertEquals("test after list.set(index, element)...", getList4(), list);

        list.add(0, getNewObject());
        list.add(list.size() / 2, getNewObject());
        list.add(list.size() - 1, getNewObject());

        assertEquals("test after list.add(index.element)...", getList5(), list);
        assertEquals("test list.removeHead(0)...", "Object9", list.remove(0));
        assertEquals("test list.removeHead(size/2)...", "Object4", list.remove(list.size() / 2));
        assertEquals("test list.removeHead(size-1)...", "Object7", list.remove(list.size() - 1));
        assertEquals("test after list.removeHead(index.element)...", getList6(), list);
        assertEquals("test list.indexOf(Object8)...", 0, list.indexOf("Object8"));
        assertEquals("test list.indexOf(Object10)...", 2, list.indexOf("Object10"));
        assertEquals("test list.indexOf(Object11)...", 5, list.indexOf("Object11"));

        list.add("Object1");
        list.add("Object6");

        assertEquals("test list.lastIndexOf(Object1)...", 6, list.lastIndexOf("Object1"));
        assertEquals("test list.iterator()...", 0, list.listIterator().nextIndex());
        assertEquals("test list.iterator(index)...", 6, list.listIterator(6).nextIndex());
        assertEquals("test list.subList(2, 5)...", getList7(), list.subList(2, 5));

        printTestResult();
    }

    private static List<Object> getList() {
        List<Object> list = new ArrayList<>();
        getList(list);
        return list;
    }

    private static void getList(List<Object> list) {
        for (int i = 0; i < 3; i++) {
            list.add(getNewObject());
        }
    }

    public static Object getNewObject() {
        return "Object" + countNewObjects++;
    }

    public static List<Object> getList3() {
        List<Object> list = new ArrayList<>();
        list.add("Object0");
        list.add("Object1");
        list.add("Object3");
        list.add("Object4");
        list.add("Object5");
        list.add("Object2");
        return list;
    }

    public static List<Object> getList4() {
        List<Object> list = new ArrayList<>();
        list.add("Object8");
        list.add("Object1");
        list.add("Object6");
        list.add("Object4");
        list.add("Object5");
        list.add("Object7");
        return list;
    }

    public static List<Object> getList5() {
        List<Object> list = new ArrayList<>();
        list.add("Object9");
        list.add("Object8");
        list.add("Object1");
        list.add("Object10");
        list.add("Object6");
        list.add("Object4");
        list.add("Object5");
        list.add("Object11");
        list.add("Object7");
        return list;
    }

    public static List<Object> getList6() {
        List<Object> list = new ArrayList<>();
        list.add("Object8");
        list.add("Object1");
        list.add("Object10");
        list.add("Object6");
        list.add("Object5");
        list.add("Object11");
        return list;
    }

    public static List<Object> getList7() {
        List<Object> list = new ArrayList<>();
        list.add("Object10");
        list.add("Object6");
        list.add("Object5");
        return list;
    }
}
