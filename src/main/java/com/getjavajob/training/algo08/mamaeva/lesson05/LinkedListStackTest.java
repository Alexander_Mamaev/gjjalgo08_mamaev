package com.getjavajob.training.algo08.mamaeva.lesson05;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.util.Assert.*;

public class LinkedListStackTest {
    private static LinkedListStack<Object> stack = new LinkedListStack<>();
    private static List<Object> list = new LinkedList<>();
    private static int countObjects;

    public static void main(String[] args) {
        fillStack();
        testStack();
        testPopWithException();

        printTestResult();
    }

    private static void testStack() {
        int size = list.size();
        while (stack.size() > 0) {
            assertEquals("test stack.push() stack.pop()...", list.get(--size), stack.pop());
        }
    }

    private static void fillStack() {
        for (int i = 0; i < 5; i++) {
            Object obj = getNewObject();
            stack.push(obj);
            list.add(obj);
        }
    }

    private static void testPopWithException() {
        try {
            stack.pop();
            fail("testPopWithException...missed expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("Test stack.pop() with exception...", NoSuchElementException.class, e.getClass());
        }
    }

    private static String getNewObject() {
        return "Object" + countObjects++;
    }
}
