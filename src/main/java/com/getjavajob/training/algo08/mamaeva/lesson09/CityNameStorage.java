package com.getjavajob.training.algo08.mamaeva.lesson09;

import java.util.Collection;
import java.util.TreeSet;

public class CityNameStorage {
    private static TreeSet<String> data = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    public boolean add(String name) {
        return data.add(name);
    }

    public Collection<String> getCityNamesStartWith(String nameStartWith) {
        return data.subSet(nameStartWith, nameStartWith + Character.MAX_VALUE);
    }
}
