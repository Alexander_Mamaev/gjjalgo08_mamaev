package com.getjavajob.training.algo08.mamaeva.lesson01;

import static com.getjavajob.training.algo08.mamaeva.lesson01.Task07.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class Task07Test {
    public static void main(String[] args) {
        swapTests(-12, -31);
        swapTests(-12, 31);
        swapTests(45, -23);
        swapTests(7, 101);
        printTestResult();
    }

    private static void swapTests(int a, int b) {
        System.out.printf("Swap %s and %s%n%n", a, b);
        testSwapNumbers1(a, b, new int[]{b, a});
        testSwapNumbers2(a, b, new int[]{b, a});
        testSwapNumbers3(a, b, new int[]{b, a});
        testSwapNumbers4(a, b, new int[]{b, a});
    }

    private static void testSwapNumbers1(int a, int b, int[] expectedResult) {
        assertEquals("Task07Test:", expectedResult, swapNumbers1(a, b));
        System.out.println();
    }

    private static void testSwapNumbers2(int a, int b, int[] expectedResult) {
        assertEquals("Task07Test:", expectedResult, swapNumbers2(a, b));
        System.out.println();
    }

    private static void testSwapNumbers3(int a, int b, int[] expectedResult) {
        assertEquals("Task07Test:", expectedResult, swapNumbers3(a, b));
        System.out.println();
    }

    private static void testSwapNumbers4(int a, int b, int[] expectedResult) {
        assertEquals("Task07Test:", expectedResult, swapNumbers4(a, b));
        System.out.println();
    }
}
