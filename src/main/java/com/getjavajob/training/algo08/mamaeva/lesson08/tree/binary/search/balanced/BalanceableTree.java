package com.getjavajob.training.algo08.mamaeva.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;
import com.getjavajob.training.algo08.mamaeva.lesson08.tree.binary.search.BinarySearchTree;

public class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (makeLeftChild) {
            NodeImpl<E> left = child.left;
            parent.right = left;
            if (left != null) {
                left.parent = parent;
            }
            child.left = parent;
            child.parent = parent.parent;
            if (parent.parent != null) {
                if (parent.parent.left == parent) {
                    parent.parent.left = child;
                } else {
                    parent.parent.right = child;
                }
            }
            parent.parent = child;
        } else {
            NodeImpl<E> right = child.right;
            parent.left = right;
            if (right != null) {
                right.parent = parent;
            }
            child.right = parent;
            child.parent = parent.parent;
            if (parent.parent != null) {
                if (parent.parent.left == parent) {
                    parent.parent.left = child;
                } else {
                    parent.parent.right = child;
                }
            }
            parent.parent = child;
        }
        setRoot();
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    public void rotate(Node<E> n) {
        NodeImpl<E> node = validate(n);
        NodeImpl<E> parent = validate(node.parent);
        if (parent.right == node) {
            relink(parent, node, true);
        } else {
            relink(parent, node, false);
        }
    }

    /**
     * Performs a left-right/right-left rotations.
     */
    public Node<E> rotateTwice(Node<E> n) {
        rotate(n);
        rotate(n);
        return n;
    }

    private void setRoot() {
        NodeImpl<E> node = validate(root());
        while (node.parent != null) {
            node = node.parent;
        }
        root = node;
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        if (parent(n) != null && grandparent(n) != null && uncle(n) == null) {
            if ((right(parent(n)) == n && left(grandparent(n)) == parent(n))
                    || (left(parent(n)) == n && right(grandparent(n)) == parent(n))) {
                rotateTwice(n);
                return n;
            }
            if ((left(parent(n)) == n && left(grandparent(n)) == parent(n))
                    || (right(parent(n)) == n && right(grandparent(n)) == parent(n))) {
                rotate(parent(n));
                return parent(n);
            }
        }
        return null;
    }
}
