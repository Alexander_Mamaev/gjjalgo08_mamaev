package com.getjavajob.training.algo08.mamaeva.lesson06;

import java.util.Objects;

public class AssociativeArray<K, V> {
    private final static int DEFAULT_INITIAL_CAPACITY = 4;
    private final static float DEFAULT_LOAD_FACTOR = 0.75f;
    private int capacity;
    private int size;
    private float loadFactor;
    private Entry<K, V>[] table;


    public AssociativeArray() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    public AssociativeArray(int initialCapacity, float loadFactor) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        }
        if (loadFactor <= 0 || Float.isNaN(loadFactor)) {
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
        }
        this.loadFactor = loadFactor;
        capacity = initialCapacity;
        table = new Entry[capacity];
    }

    public V add(K key, V val) {
        Objects.requireNonNull(val, "value == null");
        if (key == null) {
            return addNullKey(val);
        }
        if (size >= loadFactor * capacity) {
            capacity *= 2;
            rehashTable();
        }
        int bucketIndex = getBucketIndex(key);
        for (Entry<K, V> e = table[bucketIndex]; e != null; e = e.next) {
            Object k;
            if ((k = e.key) == key || key.equals(k)) {
                V oldValue = e.value;
                e.value = val;
                return oldValue;
            }
        }
        table[bucketIndex] = new Entry<>(key, val, table[bucketIndex], bucketIndex);
        size++;
        return null;
    }

    private void rehashTable() {
        Entry<K, V>[] newTable = new Entry[capacity];
        for (Entry<K, V> e : table) {
            while (e != null) {
                Entry<K, V> next = e.next;
                int bucketIndex = null == e.key ? 0 : getBucketIndex(e.key);
                e.hash = bucketIndex;
                e.next = newTable[bucketIndex];
                newTable[bucketIndex] = e;
                e = next;
            }
        }
        table = newTable;
    }

    private V addNullKey(V val) {
        if (table[0] == null) {
            table[0] = new Entry<>(null, val, null, 0);
            return null;
        }
        V oldValue = null;
        for (Entry<K, V> e = table[0]; e != null; e = e.next) {
            oldValue = e.value;
            e.value = val;
        }
        return oldValue;
    }

    public int getBucketIndex(K key) {
        return (key.hashCode() >>> 1) % capacity;
    }

    public V get(K key) {
        Entry<K, V> entry = getEntry(key);
        return null == entry ? null : entry.value;
    }

    private Entry<K, V> getEntry(K key) {
        int bucketIndex = (key == null) ? 0 : getBucketIndex(key);
        for (Entry<K, V> e = table[bucketIndex]; e != null; e = e.next) {
            Object k;
            if (e.hash == bucketIndex && ((k = e.key) == key || (key != null && key.equals(k)))) {
                return e;
            }
        }
        return null;
    }

    public V remove(K key) {
        Entry<K, V> e = removeEntryForKey(key);
        return (e == null ? null : e.value);
    }

    private Entry<K, V> removeEntryForKey(K key) {
        int bucketIndex = (key == null) ? 0 : getBucketIndex(key);
        Entry<K, V> prev = table[bucketIndex];
        Entry<K, V> e = prev;
        while (e != null) {
            Entry<K, V> next = e.next;
            Object k;
            if (e.hash == bucketIndex && ((k = e.key) == key || (key != null && key.equals(k)))) {
                if (prev == e) {
                    table[bucketIndex] = next;
                } else {
                    prev.next = next;
                }
                size--;
                return e;
            }
            prev = e;
            e = next;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry e : table) {
            if (e != null) {
                while (e != null) {
                    String key = null == e.key ? null : (String) e.key;
                    sb.append(key).append(" - ").append(e.value).append("; ");
                    e = e.next;
                }
                sb.append(System.lineSeparator());
            }
        }
        return sb.toString();
    }

    private static class Entry<K, V> {
        private K key;
        private V value;
        private Entry<K, V> next;
        private int hash;

        public Entry(K key, V value, Entry<K, V> next, int hash) {
            this.key = key;
            this.value = value;
            this.next = next;
            this.hash = hash;
        }
    }
}
