package com.getjavajob.training.algo08.mamaeva.lesson09;

import com.getjavajob.training.algo08.util.StopWatch;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class CityNameStorageTest {
    private static StopWatch time = new StopWatch();

    public static void main(String[] args) {
        cityNameStorageTest();
        implementationsComparisonTest();
    }

    private static void cityNameStorageTest() {
        CityNameStorage data = new CityNameStorage();
        data.add("Yekaterinburg");
        data.add("Mogilev");
        data.add("Novosibirsk");
        data.add("mOscow");
        data.add("newTown");
        data.add("ScowMo");
        data.add("Boston");
        data.add("newMoTown");
        data.add("something");
        data.add("Mozyr");
        data.add("zzzombzyzzz");

        assertEquals("test storage of city names...", "[Mogilev, mOscow, Mozyr]", data.getCityNamesStartWith("mo").toString());
        assertEquals("test storage of city names...", "[newMoTown, newTown, Novosibirsk]", data.getCityNamesStartWith("n").toString());
        assertEquals("test storage of city names...", "[Novosibirsk]", data.getCityNamesStartWith("no").toString());
        assertEquals("test storage of city names...", "[newMoTown, newTown]", data.getCityNamesStartWith("ne").toString());
        assertEquals("test storage of city names...", "[zzzombzyzzz]", data.getCityNamesStartWith("z").toString());
    }

    // -verbose:gc -XX:+PrintGCTimeStamps -Xms8G
    private static void implementationsComparisonTest() {
        CityNameStorage storage = fillStorage();
        performanceTest(storage); // 4 ms
    }

    private static CityNameStorage fillStorage() {
        CityNameStorage storage = new CityNameStorage();
        for (int i = 0; i < 1_000_000; i++) {
            storage.add(String.valueOf(i));
        }
        return storage;
    }

    private static void performanceTest(CityNameStorage storage) {
        System.out.println("------------Start--------------");
        time.start();
        for (int j = 0; j < 1_000; j++) {
            storage.getCityNamesStartWith("5");
        }
        System.out.printf("1 - storage.getCityNamesStartWith(\"5\") - %d ms" + System.lineSeparator(), time.getElapsedTime());
        System.out.println("  - number of elements - " + storage.getCityNamesStartWith("5").size());
    }
}
