package com.getjavajob.training.algo08.mamaeva.lesson05;

import static com.getjavajob.training.algo08.mamaeva.lesson05.ExpressionCalculator.calculatePostfix;
import static com.getjavajob.training.algo08.mamaeva.lesson05.ExpressionCalculator.convertInfixToPostfix;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class ExpressionCalculatorTest {
    public static void main(String[] args) throws NoSuchMethodException {
        String ex = "5+((1+2)*4)-3"; //RPN 5 1 2 + 4 * + 3 -  result 14
        testCalculator(ex, new String[]{"5", "1", "2", "+", "4", "*", "+", "3", "-"}, 14);
        ex = "(1+2)*4+3"; //RPN 1 2 + 4 * 3 +  result 15
        testCalculator(ex, new String[]{"1", "2", "+", "4", "*", "3", "+"}, 15);
        ex = "(8+2*5)/(1+3*2-4)"; //RPN 8 2 5 * + 1 3 2 * + 4 - /   result 6
        testCalculator(ex, new String[]{"8", "2", "5", "*", "+", "1", "3", "2", "*", "+", "4", "-", "/"}, 6);
        ex = "(15/3+11-3*5)/3.2*(5.6-10)"; // RPN 15 3 / 11 + 3 5 * — 3.2 / 5.6 10 — *  result -1.375
        testCalculator(ex, new String[]{"15", "3", "/", "11", "+", "3", "5", "*", "-", "3.2", "/", "5.6", "10", "-", "*"}, -1.375);
    }

    private static void testCalculator(String ex, String[] exPostfix, double result) throws NoSuchMethodException {
        assertEquals(ex + " expression convert to postfix notation...", exPostfix, convertInfixToPostfix(ex).toArray());
        assertEquals(ex + " test postfix calculation...", result, calculatePostfix(ex));
    }
}
