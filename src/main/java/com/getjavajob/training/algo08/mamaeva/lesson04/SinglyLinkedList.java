package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.NoSuchElementException;

public class SinglyLinkedList<E> {
    private Node<E> head;
    private Node<E> tail;
    private int size;

    /**
     * add node to tail
     */
    public void add(E e) {
        if (head == null) {
            head = new Node<>(e, tail);
        } else if (tail == null) {
            tail = new Node<>(e, null);
            head.next = tail;
        } else {
            Node<E> x = tail;
            tail = new Node<>(e, null);
            x.next = tail;
        }
        size++;
    }

    /**
     * add node to head
     */
    public void addHead(E e) {
        if (head == null) {
            head = new Node<>(e, null);
        } else {
            Node<E> x = head;
            head = new Node<>(e, x);
        }
        size++;
    }

    /**
     * remove node from head
     */
    public E removeHead() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        E val = head.val;
        head = head.next;
        size--;
        return val;
    }

    public Node<E> get(int index) {
        checkElementIndex(index);
        Node<E> x = head;
        for (int i = 0; i < index; i++) {
            x = x.next;
        }
        return x;
    }

    public E set(int index, E e) {
        checkElementIndex(index);
        Node<E> x = get(index);
        E oldVal = x.val;
        x.val = e;
        return oldVal;
    }

    public void reverse() {
        Node<E> currentNode = head;
        while (currentNode.next != null) {
            swap(currentNode);
        }
    }

    private void swap(Node<E> currentNode) {
        Node<E> tmp = head;
        head = currentNode.next;
        currentNode.next = head.next;
        head.next = tmp;
    }

    public int size() {
        return size;
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        for (int i = 0; i < size; i++) {
            array[i] = get(i).val;
        }
        return array;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private static class Node<E> {
        Node<E> next;
        E val;

        public Node(E e, Node<E> next) {
            val = e;
            this.next = next;
        }

        @Override
        public String toString() {
            return (String) val;
        }
    }
}
