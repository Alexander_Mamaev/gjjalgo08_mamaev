package com.getjavajob.training.algo08.mamaeva.lesson08.tree.binary.search;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;
import com.getjavajob.training.algo08.mamaeva.lesson07.tree.binary.LinkedBinaryTree;

import java.util.Comparator;

public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;
    private int size;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1 the object to be compared
     * @param val2 the object to be compared
     * @return result of comparing using the correct comparison method (1 - if val1>val2, 0 - if val1==val2, -1 - if val1<val2)
     */
    protected int compare(E val1, E val2) {
        return comparator == null ? ((Comparable<E>) val1).compareTo(val2) : comparator.compare(val1, val2);
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n   comparable node
     * @param val comparable node.element
     * @return node with node.element == val
     */
    protected Node<E> treeSearch(Node<E> n, E val) {
        NodeImpl<E> node = validate(n);
        E element = node.getElement();
        switch (compare(element, val)) {
            case 1:
                if (node.left != null) {
                    return treeSearch(node.left, val);
                }
                break;
            case -1:
                if (node.right != null) {
                    return treeSearch(node.right, val);
                }
                break;
            case 0:
                return node;
        }
        return null;
    }

    public Node<E> treeSearch(E val) {
        return treeSearch(root(), val);
    }

    public Node<E> add(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> addedNode = (NodeImpl<E>) n;
        if (root == null) {
            root = addedNode;
            size++;
            return root;
        }
        NodeImpl<E> node = root;
        boolean flag = true;
        while (flag) {
            switch (compare(node.getElement(), addedNode.getElement())) {
                case 1:
                    if (node.left != null) {
                        node = node.left;
                    } else {
                        addedNode.parent = node;
                        node.left = addedNode;
                        flag = false;
                    }
                    break;
                case -1:
                    if (node.right != null) {
                        node = node.right;
                    } else {
                        addedNode.parent = node;
                        node.right = addedNode;
                        flag = false;
                    }
                    break;
                case 0:
                    return null;
            }
        }
        size++;
        afterElementAdded(addedNode);
        return addedNode;
    }

    public Node<E> add(E e) throws IllegalArgumentException {
        NodeImpl<E> node = new NodeImpl<>(e);
        return add(node);
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public E remove(E val) throws IllegalArgumentException {
        if (val == null) {
            return null;
        }
        NodeImpl<E> removedNode = (NodeImpl<E>) treeSearch(val);
        return removedNode == null ? null : remove(removedNode);
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> removedNode = (NodeImpl<E>) n;
        E element = removedNode.getElement();
        NodeImpl<E> p = removedNode.parent;
        NodeImpl<E> replacedNode;
        if (removedNode.left == null && removedNode.right == null) {
            if (p == null) {
                root = null;
                size--;
                return element;
            } else if (p.right == removedNode) {
                afterElementRemoved(removedNode);
                p.right = null;
            } else if (p.left == removedNode) {
                afterElementRemoved(removedNode);
                p.left = null;
            }
            removedNode.parent = null;
        } else if (removedNode.left != null) {
            replacedNode = maxOfSubTree(removedNode.left);
            if (replacedNode.left != null) {
                if (replacedNode.parent.right == replacedNode) {
                    replacedNode.parent.right = replacedNode.left;
                } else if (replacedNode.parent.left == replacedNode) {
                    replacedNode.parent.left = replacedNode.left;
                }
                replacedNode.left.parent = replacedNode.parent;
                afterElementRemoved(replacedNode);
            } else if (replacedNode.parent.left == replacedNode) {
                afterElementRemoved(replacedNode);
                replacedNode.parent.left = null;
            } else if (replacedNode.parent.right == replacedNode) {
                afterElementRemoved(replacedNode);
                replacedNode.parent.right = null;
            }
            set(removedNode, replacedNode.getElement());
            clearNodeLinks(replacedNode);
        } else {
//        if (removedNode.left == null && removedNode.right != null) {
            replacedNode = minOfSubTree(removedNode.right);
            if (replacedNode.right != null) {
                if (replacedNode.parent.right == replacedNode) {
                    replacedNode.parent.right = replacedNode.right;
                } else if (replacedNode.parent.left == replacedNode) {
                    replacedNode.parent.left = replacedNode.right;
                }
                replacedNode.right.parent = replacedNode.parent;
            } else if (replacedNode.parent.left == replacedNode) {
                afterElementRemoved(replacedNode);
                replacedNode.parent.left = null;
            } else if (replacedNode.parent.right == replacedNode) {
                afterElementRemoved(replacedNode);
                replacedNode.parent.right = null;
            }
            set(removedNode, replacedNode.getElement());
            clearNodeLinks(replacedNode);
        }
        size--;
        return element;
    }

    private NodeImpl<E> minOfSubTree(NodeImpl<E> node) {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }

    private void clearNodeLinks(NodeImpl<E> node) {
        node.parent = null;
        node.left = null;
        node.right = null;
    }

    private NodeImpl<E> maxOfSubTree(NodeImpl<E> node) {
        while (node.right != null) {
            node = node.right;
        }
        return node;
    }

    protected void afterElementRemoved(Node<E> n) {
    }

    protected void afterElementAdded(Node<E> n) {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        NodeImpl<E> root = validate(root());
        sb.append("(").append(root.getElement());
        printElement(root, sb);
        sb.append(")");
        return sb.toString();
    }

    private void printElement(NodeImpl<E> n, StringBuilder sb) {
        NodeImpl<E> leftChild = n.left;
        NodeImpl<E> rightChild = n.right;
        if (leftChild != null) {
            sb.append("(").append(leftChild.getElement());
            printElement(leftChild, sb);
            if (rightChild == null) {
                sb.append(", _)");
            }
        } else if (rightChild != null) {
            sb.append("(_");
        }
        if (rightChild != null) {
            sb.append(", ").append(rightChild.getElement());
            printElement(rightChild, sb);
            sb.append(")");
        }
    }
}
