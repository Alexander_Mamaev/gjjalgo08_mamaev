package com.getjavajob.training.algo08.mamaeva.lesson06;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class MapTest {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        Map<String, String> map2 = new HashMap<>();

        assertEquals("test map.isEmpty()...", true, map.isEmpty());
        map.put(null, "value_null");
        assertEquals("test map.isEmpty()...", false, map.isEmpty());
        assertEquals("test add(null, value)...", "value_null", map.put(null, "value_null_2"));
        map.put("polygenelubricants", "Value_polygenelubricants");
        assertEquals("test add..", null, map.put("GydZG_", "Value_GydZG_"));
        assertEquals("test add..", null, map.put("DESIGNING WORKHOUSES", "Value_DSIGNING WORKHOUSES"));
        mapTest(map);
        assertEquals("test map.remove(key)...", "newValue_3", map.remove("key_3"));
        assertEquals("test map.containsKey(key)...", true, map.containsKey("key_6"));
        assertEquals("test map.containsKey(key)...", false, map.containsKey("key_100"));
        assertEquals("test map.containsValue(value)...", true, map.containsValue("newValue_4"));
        assertEquals("test map.containsValue(value)...", false, map.containsValue("newValue_45"));
        map2.putAll(map);
        assertEquals("test equals and map.putAll(map)...", map, map2);
        map2.clear();
        assertEquals("test map.clear()", true, map2.isEmpty());
        keySetTest(map);
        valuesTest(map);

        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        String expectedEntrySet = "[null=value_null, key_7=value_7, key_6=value_6, key_13=value_13, key_5=value_5, key_14=value_14, key_4=newValue_4, key_15=value_15, key_2=newValue_2, key_1=newValue_1, key_0=newValue_0, polygenelubricants=Value_polygenelubricants, GydZG_=Value_GydZG_, DESIGNING WORKHOUSES=Value_DSIGNING WORKHOUSES]";
        assertEquals("test map.entrySet()...", expectedEntrySet, Arrays.toString(entrySet.toArray()));
        System.out.println(Arrays.toString(entrySet.toArray()));

        printTestResult();
    }

    private static void valuesTest(Map<String, String> map) {
        Collection<String> collectionValues = map.values();
        String[] expectedValues = {"value_null", "value_7", "value_6", "value_13", "value_5", "value_14", "newValue_4",
                "value_15", "newValue_2", "newValue_1", "newValue_0", "Value_polygenelubricants",
                "Value_GydZG_", "Value_DSIGNING WORKHOUSES"};
        assertEquals("test map.values()...", expectedValues, collectionValues.toArray());
    }

    private static void keySetTest(Map<String, String> map) {
        Set<String> set = map.keySet();
        String[] expectedSet = {null, "key_7", "key_6", "key_13", "key_5", "key_14", "key_4", "key_15", "key_2",
                "key_1", "key_0", "polygenelubricants", "GydZG_", "DESIGNING WORKHOUSES"};
        assertEquals("test map.keySet()...", expectedSet, set.toArray());
    }

    private static void mapTest(Map<String, String> map) {
        for (int i = 0; i < 16; i++) {
            map.put("key_" + i, "value_" + i);
        }
        for (int i = 8; i < 13; i++) {
            map.remove("key_" + i);
        }
        for (int i = 0; i < 5; i++) {
            map.put("key_" + i, "newValue_" + i);
        }
        map.put(null, "value_null");
        String[] valuesExpected = {
                "newValue_0",
                "newValue_1",
                "newValue_2",
                "newValue_3",
                "newValue_4",
                "value_5",
                "value_6",
                "value_7",
                null,
                null,
                null,
                null,
                null,
                "value_13",
                "value_14",
                "value_15",
                null,
                null
        };
        for (int i = 0; i < 18; i++) {
            assertEquals("test add, remove, get...", valuesExpected[i], map.get("key_" + i));
        }
    }
}
