package com.getjavajob.training.algo08.mamaeva.lesson07.tree.binary;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import static com.getjavajob.training.algo08.util.Assert.*;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree<String> lbt = new LinkedBinaryTree<>();
        Node<String> root = lbt.addRoot("root");
        Node<String> n1 = lbt.addLeft(root, "n1");
        Node<String> n2 = lbt.addRight(root, "n2");
        Node<String> n11 = lbt.addLeft(n1, "n11");
        Node<String> n12 = lbt.addRight(n1, "n12");
        Node<String> n121 = lbt.addLeft(n12, "n121");
        Node<String> n1212 = lbt.addRight(n121, "n1212");
        Node<String> n21 = lbt.addLeft(n2, "n21");
        Node<String> n22 = lbt.addRight(n2, "n22");
        Node<String> n212 = lbt.addRight(n21, "n212");

        testIterator(lbt);
        testPostOrder(lbt);
        testPreOrder(lbt);
        testBreadthFirst(lbt);
        testInOrder(lbt);

        assertEquals("test addRoot(), root()...", root, lbt.root());
        testRoot(lbt, root);
        assertEquals("test addLeft(Node, e), left()...", n21, lbt.left(n2));
        testAddLeft(lbt, n1);
        assertEquals("test addRight(Node, e), right()...", n12, lbt.right(n1));
        testAddRight(lbt, n1);
        testAdd(lbt, n212);
        assertEquals("test parent(Node)...", n1, lbt.parent(n12));
        assertEquals("test parent(Root)...", null, lbt.parent(root));
        assertEquals("test set(Node, e)...", "n1212", lbt.set(n1212, "n_1212"));
        assertEquals("test set(Node, e)...", "n_1212", lbt.set(n1212, "n1212"));
        assertEquals("test size()...", 12, lbt.size());
        testRemove(lbt, n121, n212);

        printTestResult();
    }

    private static void testPostOrder(LinkedBinaryTree<String> lbt) {
        StringBuilder actual = new StringBuilder();
        Iterable<Node<String>> coll = lbt.postOrder();
        for (Node<String> aLbt : coll) {
            actual.append(aLbt.getElement()).append("/");
        }
        String expected = "n11/n1212/n121/n12/n1/n212/n21/n22/n2/root/";
        assertEquals("test postOrder()...", expected, actual.toString());
    }

    private static void testPreOrder(LinkedBinaryTree<String> lbt) {
        StringBuilder actual = new StringBuilder();
        Iterable<Node<String>> coll = lbt.preOrder();
        for (Node<String> aLbt : coll) {
            actual.append(aLbt.getElement()).append("/");
        }
        String expected = "root/n1/n11/n12/n121/n1212/n2/n21/n212/n22/";
        assertEquals("test preOrder()...", expected, actual.toString());
    }

    private static void testBreadthFirst(LinkedBinaryTree<String> lbt) {
        StringBuilder actual = new StringBuilder();
        Iterable<Node<String>> coll = lbt.breadthFirst();
        for (Node<String> aLbt : coll) {
            actual.append(aLbt.getElement()).append("/");
        }
        String expected = "root/n1/n2/n11/n12/n21/n22/n121/n212/n1212/";
        assertEquals("test breadthFirst()...", expected, actual.toString());
    }

    private static void testInOrder(LinkedBinaryTree<String> lbt) {
        StringBuilder actual = new StringBuilder();
        Iterable<Node<String>> coll = lbt.inOrder();
        for (Node<String> aLbt : coll) {
            actual.append(aLbt.getElement()).append("/");
        }
        String expected = "n11/n1/n121/n1212/n12/root/n21/n212/n2/n22/";
        assertEquals("test inOrder()...", expected, actual.toString());
    }

    private static void testRemove(LinkedBinaryTree<String> lbt, Node<String> n121, Node<String> n212) {
        assertEquals("test remove(Node)...", "n212", lbt.remove(n212));
        assertEquals("test remove(Node)...", "n121", lbt.remove(n121));

        StringBuilder actual = new StringBuilder();
        for (String aLbt : lbt) {
            actual.append(aLbt).append("/");
        }
        String expected = "root/n1/n11/n12/n2/n21/n22/";
        assertEquals("test iterator() after remove(Node)...", expected, actual.toString());
    }

    private static void testIterator(LinkedBinaryTree<String> lbt) {
        StringBuilder actual = new StringBuilder();
        for (String aLbt : lbt) {
            actual.append(aLbt).append("/");
        }
        String expected = "root/n1/n11/n12/n121/n1212/n2/n21/n212/n22/";
        assertEquals("test iterator(), nodes()...", expected, actual.toString());
    }

    private static void testAdd(LinkedBinaryTree<String> lbt, Node<String> n212) {
        Node<String> n2121 = lbt.add(n212, "n2121");
        Node<String> n2122 = lbt.add(n212, "n2122");
        try {
            lbt.add(n212, "n2121");
            fail("test add(Node, e) with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test add(Node, e) with exception...", "the node has left and right child already", e.getMessage());
        }
        try {
            lbt.add(n212, "n2122");
            fail("test add(Node, e) with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test add(Node, e) with exception...", "the node has left and right child already", e.getMessage());
        }
    }

    private static void testAddRight(LinkedBinaryTree<String> lbt, Node<String> n1) {
        try {
            lbt.addRight(n1, "n1");
            fail("test addRight() with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test addRoot() with exception...", "the right child is already exist", e.getMessage());
        }
    }

    private static void testAddLeft(LinkedBinaryTree<String> lbt, Node<String> n1) {
        try {
            lbt.addLeft(n1, "n1");
            fail("test addLeft() with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test addRoot() with exception...", "the left child is already exist", e.getMessage());
        }
    }

    private static void testRoot(LinkedBinaryTree<String> lbt, Node<String> root) {
        try {
            lbt.addRoot("newRoot");
            fail("test addRoot() with exception...expected IllegalStateException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalStateException e) {
            assertEquals("test addRoot() with exception...", "the root is already exist", e.getMessage());
        }
    }
}
