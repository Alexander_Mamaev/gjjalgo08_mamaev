package com.getjavajob.training.algo08.mamaeva.lesson07.tree.binary;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.AbstractTree;
import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        if (parent(n) != null) {
            if (left(parent(n)) == n) {
                return right(parent(n));
            } else {
                return left(parent(n));
            }
        }
        return null;
    }

    protected Node<E> grandparent(Node<E> n) throws IllegalArgumentException {
        if (parent(n) != null && parent(parent(n)) != null) {
            return parent(parent(n));
        }
        return null;
    }

    protected Node<E> uncle(Node<E> n) throws IllegalArgumentException {
        if (grandparent(n) != null) {
            if (parent(n) == left(grandparent(n)) && right(grandparent(n)) != null) {
                return right(grandparent(n));
            } else if (parent(n) == right(grandparent(n)) && left(grandparent(n)) != null) {
                return left(grandparent(n));
            }
        }
        return null;
    }

    @Override
    public Iterable<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Collection<Node<E>> col = new ArrayList<>();
        col.add(left(n));
        col.add(right(n));
        return col;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        int count = 0;
        if (left(n) != null) {
            count++;
        }
        if (right(n) != null) {
            count++;
        }
        return count;
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Iterable<Node<E>> inOrder() {
        Collection<Node<E>> coll = new ArrayList<>();
        inOrder(root(), coll);
        return coll;
    }

    private void inOrder(Node<E> n, Collection<Node<E>> coll) {
        if (left(n) != null) {
            inOrder(left(n), coll);
        }
        coll.add(n);
        if (right(n) != null) {
            inOrder(right(n), coll);
        }
    }
}
