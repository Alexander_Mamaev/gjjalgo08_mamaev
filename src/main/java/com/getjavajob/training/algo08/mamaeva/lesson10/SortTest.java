package com.getjavajob.training.algo08.mamaeva.lesson10;

import com.getjavajob.training.algo08.util.StopWatch;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * QuickSort for Integer.MAX_VALUE-2 elements needed: 360689 ms, 8 Gb for array;
 * <p>
 * comparison of QuickSort and MergeSort for Integer.MAX_Value/10 random elements:
 * QuickSort =  32_397 ms, 0.8 Gb for array;
 * MergeSort = 53_914 ms, 0.8 Gb for array + 0.8 Gb for merge;
 */

public class SortTest {
    private static StopWatch time = new StopWatch();

    public static void main(String[] args) {
        testBubbleSortSimple();
        testInsertionSortSimple();
        testQuickSortSimple();
        testMergeSortSimple();
        testQuickSortMax();
        testMergeSortMax();
    }

    private static void testBubbleSortSimple() {
        int[] array = {3, 7, 7, 4, 1, 0, 9, 6, 2, 2, 8, 6, 5};
        int[] expectedArray = {0, 1, 2, 2, 3, 4, 5, 6, 6, 7, 7, 8, 9};
        Sort.bubbleSort(array);
        assertEquals("test bubbleSort(array)...", expectedArray, array);
    }

    private static void testInsertionSortSimple() {
        int[] array = {3, 7, 7, 4, 1, 0, 9, 6, 2, 2, 8, 6, 5};
        int[] expectedArray = {0, 1, 2, 2, 3, 4, 5, 6, 6, 7, 7, 8, 9};
        Sort.insertionSort(array);
        assertEquals("test insertionSort(array)...", expectedArray, array);
    }

    private static void testQuickSortSimple() {
        int[] array = {3, 7, 7, 4, 1, 0, 9, 6, 2, 2, 8, 6, 5};
        int[] expectedArray = {0, 1, 2, 2, 3, 4, 5, 6, 6, 7, 7, 8, 9};
        Sort.quickSort(array);
        assertEquals("test quickSort(array)...", expectedArray, array);
    }

    private static void testMergeSortSimple() {
        int[] array = {3, 7, 7, 4, 1, 0, 9, 6, 2, 2, 8, 6, 5};
        int[] expectedArray = {0, 1, 2, 2, 3, 4, 5, 6, 6, 7, 7, 8, 9};
        Sort.mergeSort(array);
        assertEquals("test testMergeSortMax(array)...", expectedArray, array);
    }

    private static void testQuickSortMax() {
        System.gc();
        int[] arr = new int[Integer.MAX_VALUE / 10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * Integer.MAX_VALUE);
        }
        time.start();
        Sort.quickSort(arr);
        System.out.println("quickSort time..." + time.getElapsedTime());
        boolean result = true;  // result of test: false if arr[i-1] > arr[i]
        for (int i = 1; i < arr.length; i++) {
            if (arr[i - 1] > arr[i]) {
                result = false;
                break;
            }
        }
        assertEquals("test quickSort(maxArraySize)...", true, result);
    }

    private static void testMergeSortMax() {
        System.gc();
        int[] arr = new int[Integer.MAX_VALUE / 10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * Integer.MAX_VALUE);
        }
        time.start();
        Sort.mergeSort(arr);
        System.out.println("testMergeSortMax time " + time.getElapsedTime());
        boolean result = true;  // result of test: false if arr[i-1] > arr[i]
        for (int i = 1; i < arr.length; i++) {
            if (arr[i - 1] > arr[i]) {
                result = false;
                break;
            }
        }
        assertEquals("test quickSort(maxArraySize)...", true, result);
    }
}
