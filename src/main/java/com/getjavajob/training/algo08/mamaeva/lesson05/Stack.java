package com.getjavajob.training.algo08.mamaeva.lesson05;

public interface Stack<E> {
    /**
     * add element to the top
     */
    void push(E e);

    /**
     * removes element from the top
     */
    E pop();
}
