package com.getjavajob.training.algo08.mamaeva.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;
import com.getjavajob.training.algo08.mamaeva.lesson08.tree.binary.search.balanced.BalanceableTree;

public class RedBlackTree<E> extends BalanceableTree<E> {
    private static final boolean RED = false;
    private static final boolean BLACK = true;

    private boolean isBlack(Node<E> n) {
        NodeRBT<E> node = (NodeRBT<E>) n;
        return node == null || node.color == BLACK;
    }

    private boolean isRed(Node<E> n) {
        NodeRBT<E> node = (NodeRBT<E>) n;
        return node != null && node.color == RED;
    }

    private void makeBlack(Node<E> n) {
        if (n != null) {
            NodeRBT<E> node = (NodeRBT<E>) n;
            node.color = BLACK;
        }
    }

    private void makeRed(Node<E> n) {
        if (n != null) {
            NodeRBT<E> node = (NodeRBT<E>) n;
            node.color = RED;
        }
    }

    @Override
    public Node<E> add(E e) throws IllegalArgumentException {
        NodeRBT<E> node = new NodeRBT<>(e);
        return super.add(node);
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        if (parent(n) != null && isRed(parent(n))) {
            if (uncle(n) != null && isRed(uncle(n))) {
                makeBlack(parent(n));
                makeBlack(uncle(n));
                makeRed(grandparent(n));
                afterElementAdded(grandparent(n));
            } else {
                if (grandparent(n) != null && uncle(n) == null || (uncle(n) != null && isBlack(uncle(n)))) {
                    if ((right(parent(n)) == n && left(grandparent(n)) == parent(n))
                            || (left(parent(n)) == n && right(grandparent(n)) == parent(n))) {
                        makeBlack(n);
                        makeRed(grandparent(n));
                        rotateTwice(n);
                    } else {
                        makeBlack(parent(n));
                        makeRed(grandparent(n));
                        rotate(parent(n));
                    }
                }
            }
        }
        makeBlack(root());
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        NodeRBT<E> node = (NodeRBT<E>) n;
        if (isBlack(node)) {
            if (right(node) != null && isRed(right(node))) {
                makeBlack(right(node));
                return;
            } else if (left(node) != null && isRed(left(node))) {
                makeBlack(left(node));
                return;
            }

            boolean isDoubleBlack = true;
            while (isDoubleBlack && parent(node) != null && sibling(node) != null) {
                isDoubleBlack = false;
                //case-1__NODE=BLACK/PARENT=BLACK/SIBLING=RED/SIBLING_CHILDES=BLACK
                if (isBlack(parent(node)) && isRed(sibling(node)) && isBlack(left(sibling(node))) && isBlack(right(sibling(node)))) {
                    makeRed(parent(node));
                    makeBlack(sibling(node));
                    rotate(sibling(node));
                    isDoubleBlack = false;
                }
                //case-2__NODE=BLACK/PARENT=?/SIBLING=BLACK/SIBLING_CHILDES=BLACK
                if (isBlack(sibling(node)) && isBlack(left(sibling(node))) && isBlack(right(sibling(node)))) {
                    //case-2a__PARENT=BLACK
                    if (isBlack(parent(node))) {
                        makeRed(sibling(node));
                        node = (NodeRBT<E>) parent(node);
                        isDoubleBlack = true;
                    } else { //case-2b__PARENT=RED
                        makeBlack(parent(node));
                        makeRed(sibling(node));
                        isDoubleBlack = false;
                    }
                }
                // work with left subtree only
                if (parent(node) != null && sibling(node) != null && right(parent(node)) == node) {

                    //case-3a__NODE=BLACK/PARENT=BLACK/SIBLING=BLACK/SIBLING_LEFT=RED/SIBLING_RIGHT=RED
                    if (isBlack(parent(node)) && isBlack(sibling(node)) && isRed(left(sibling(node))) && isRed(right(sibling(node)))) {
                        makeBlack(left(sibling(node)));
                        rotate(sibling(node));
                        isDoubleBlack = false;
                    }
                    //case-3b__NODE=BLACK/PARENT=RED/SIBLING=BLACK/SIBLING_CHILDES=RED
                    if (isRed(parent(node)) && isBlack(sibling(node)) && isRed(left(sibling(node))) && isRed(right(sibling(node)))) {
                        makeBlack(parent(node));
                        makeBlack(left(sibling(node)));
                        rotate(sibling(node));
                        isDoubleBlack = false;
                    }
                    //case-4__NODE=BLACK/PARENT=?/SIBLING=BLACK/SIBLING_LEFT=BLACK/SIBLING_RIGHT=RED
                    if (isBlack(sibling(node)) && isBlack(left(sibling(node))) && isRed(right(sibling(node)))) {
                        makeRed(sibling(node));
                        makeBlack(right(sibling(node)));
                        rotate(right(sibling(node)));
                        isDoubleBlack = true;
                    }
                    //case-5__NODE=BLACK/PARENT=?/SIBLING=BLACK/SIBLING_LEFT=RED/SIBLING_RIGHT=BLACK
                    if (isBlack(sibling(node)) && isRed(left(sibling(node))) && isBlack(right(sibling(node)))) {
                        //case-5a__PARENT=BLACK
                        if (isBlack(parent(node))) {
                            makeBlack(left(sibling(node)));
                            rotate(sibling(node));
                            isDoubleBlack = false;
                        } else { //case-5b__PARENT=RED
                            makeBlack(parent(node));
                            makeRed(sibling(node));
                            makeBlack(left(sibling(node)));
                            rotate(sibling(node));
                            isDoubleBlack = false;
                        }
                    }
                } else { // work with right subtree only - mirrored case 3 - 5

                    //case-3__NODE=BLACK/PARENT=BLACK/SIBLING=BLACK/SIBLING_LEFT=RED/SIBLING_RIGHT=RED
                    if (isBlack(parent(node)) && isBlack(sibling(node)) && isRed(left(sibling(node))) && isRed(right(sibling(node)))) {
                        makeBlack(right(sibling(node)));
                        rotate(sibling(node));
                        isDoubleBlack = false;
                    }
                    //case-3b__NODE=BLACK/PARENT=RED/SIBLING=BLACK/SIBLING_CHILDES=RED
                    if (isRed(parent(node)) && isBlack(sibling(node)) && isRed(left(sibling(node))) && isRed(right(sibling(node)))) {
                        makeBlack(parent(node));
                        makeBlack(right(sibling(node)));
                        makeRed(sibling(node));
                        rotate(sibling(node));
                        isDoubleBlack = false;
                    }
                    //case-4__NODE=BLACK/PARENT=?/SIBLING=BLACK/SIBLING_LEFT=RED/SIBLING_RIGHT=BLACK
                    if (isBlack(sibling(node)) && isRed(left(sibling(node))) && isBlack(right(sibling(node)))) {
                        makeRed(sibling(node));
                        makeBlack(left(sibling(node)));
                        rotate(left(sibling(node)));
                        isDoubleBlack = true;
                    }
                    //case-5__NODE=BLACK/PARENT=?/SIBLING=BLACK/SIBLING_LEFT=BLACK/SIBLING_RIGHT=RED
                    if (isBlack(sibling(node)) && isBlack(left(sibling(node))) && isRed(right(sibling(node)))) {
                        //case-5a__PARENT=BLACK
                        if (isBlack(parent(node))) {
                            makeBlack(right(sibling(node)));
                            rotate(sibling(node));
                            isDoubleBlack = false;
                        } else { //case-5b__PARENT=RED
                            makeBlack(parent(node));
                            makeRed(sibling(node));
                            makeBlack(right(sibling(node)));
                            rotate(sibling(node));
                            isDoubleBlack = false;
                        }
                    }
                }
                makeBlack(root());
                if (node == root()) {
                    isDoubleBlack = false;
                }
            }
        }
    }

    public String toColorString() {
        StringBuilder sb = new StringBuilder();
        NodeImpl<E> root = validate(root());
        sb.append("(").append(colorRed(root));
        printColorElement(root, sb);
        sb.append(")");
        return sb.toString();
    }

    private void printColorElement(NodeImpl<E> n, StringBuilder sb) {
        NodeImpl<E> leftChild = n.left;
        NodeImpl<E> rightChild = n.right;
        if (leftChild != null) {
            sb.append("(").append(colorRed(leftChild));
            printColorElement(leftChild, sb);
            if (rightChild == null) {
                sb.append(", _)");
            }
        } else if (rightChild != null) {
            sb.append("(_");
        }
        if (rightChild != null) {
            sb.append(", ").append(colorRed(rightChild));
            printColorElement(rightChild, sb);
            sb.append(")");
        }
    }

    private String colorRed(NodeImpl<E> n) {
        NodeRBT<E> node = (NodeRBT<E>) n;
        return isRed(node) ? "\u001B[31m" + node.getElement() + "\u001B[0m" : node.getElement().toString();
    }

    private static class NodeRBT<E> extends NodeImpl<E> {
        private boolean color = RED;

        public NodeRBT(E element) {
            super(element);
        }

        @Override
        public String toString() {
            return "NodeRBT{" +
                    "c=" + color +
                    " e=" + super.getElement() +
                    '}';
        }
    }
}
