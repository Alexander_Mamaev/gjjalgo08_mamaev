package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.ArrayList;
import java.util.Collection;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class CollectionTest {
    private static Collection<Object> col1 = new ArrayList<>();
    private static int countNewObjects;

    public static void main(String[] args) {
        assertEquals("tested col1.isEmpty()...", true, col1.isEmpty());
        assertEquals("tested col1.size()...", 0, col1.size());
        add();
        assertEquals("tested filled col1...",
                new Object[]{"newObject0", "newObject1", "newObject2", "newObject3", "newObject4"}, col1.toArray());
        assertEquals("tested col1.isEmpty()...", false, col1.isEmpty());
        assertEquals("tested col1.size()...", 5, col1.size());
        assertEquals("tested col1.contains(\"newObject2\")...", true, col1.contains("newObject2"));
        assertEquals("tested col1.contains(\"newObject-1\")...", false, col1.contains("newObject-1"));
        assertEquals("tested col1.removeHead(\"newObject0\")...", true, col1.remove("newObject0"));
        assertEquals("tested col1.removeHead(\"newObject4\")...", true, col1.remove("newObject4"));
        assertEquals("tested col1.removeHead(\"newObject2\")...", true, col1.remove("newObject2"));
        assertEquals("tested col1.removeHead(\"newObject-1\")...", false, col1.remove("newObject-1"));

        Collection<Object> col2 = getCol2();
        assertEquals("tested col1.containsAll(col2)...", true, col1.containsAll(col2));
        assertEquals("tested col1.containsAll(col3)...", false, col1.containsAll(getCol3()));
        assertEquals("tested col1.addAll(col2)...", true, col1.addAll(getCol2()));
        assertEquals("tested after col1.addAll(col2)...", getCol4(), col1);
        assertEquals("tested col1.removeAll(col2)...", true, col1.removeAll(col2));
        assertEquals("tested after col1.addAll(col2)...", new Object[]{}, col1.toArray());

        add();
        col1.add("newObject1");
        assertEquals("tested col1.removeAll(col2)...", true, col1.removeAll(col2));
        assertEquals("tested col1.removeAll(col2)...", false, col1.removeAll(col2));
        col1.add("newObject1");
        assertEquals("tested col1.retainAll(col2)...", true, col1.retainAll(col2));
        assertEquals("tested after col1.retainAll(col2)...", new Object[]{"newObject1"}, col1.toArray());
        add();
        col1.clear();
        assertEquals("tested after col1.clear()...", new Object[]{}, col1.toArray());

        printTestResult();
    }

    private static Collection<Object> getCol4() {
        Collection<Object> col4 = new ArrayList<>();
        col4.add("newObject1");
        col4.add("newObject3");
        col4.add("newObject1");
        col4.add("newObject3");
        return col4;
    }

    private static Collection<Object> getCol3() {
        Collection<Object> col3 = new ArrayList<>();
        col3.add("newObject1");
        col3.add("newObject-1");
        col3.add("newObject1");
        col3.add("newObject3");
        return col3;
    }

    private static void add() {
        for (int i = 0; i < 5; i++) {
            assertEquals("tested col1.add()...", true, col1.add(getNewObject()));
        }
    }

    private static Object getNewObject() {
        return "newObject" + countNewObjects++;
    }

    private static Collection<Object> getCol2() {
        Collection<Object> col2 = new ArrayList<>();
        col2.add("newObject1");
        col2.add("newObject3");
        return col2;
    }

}
