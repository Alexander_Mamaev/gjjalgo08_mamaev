package com.getjavajob.training.algo08.mamaeva.lesson06;

import com.getjavajob.training.algo08.util.StopWatch;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class Task2MatrixTest {
    private static int countObject;

    public static void main(String[] args) {
        StopWatch time = new StopWatch();

        Task2Matrix<String> map = new Task2Matrix<>();
        int j = 1_000_000;
        String testValue_50_000 = null;
        String testValue_500_000 = null;
        String testValue_999_000 = null;

        time.start();
        for (int i = 0; i < 1_000_000; i++) {
            String object = getObject();
            map.set(i, j, object);
            if (i == 50_000) {
                testValue_50_000 = object;
            }
            if (i == 500_000) {
                testValue_500_000 = object;
            }
            if (i == 999_000) {
                testValue_999_000 = object;
            }
        }
        long elapsedTime = time.getElapsedTime();

        System.out.println(map);
        assertEquals("test map.size() after 1_000_000 elements added...", 1_000_000, map.size());
        assertEquals("test map.get(50_000)...", testValue_50_000, map.get(50_000, 1_000_000));
        assertEquals("test map.get(500_000)...", testValue_500_000, map.get(500_000, 1_000_000));
        assertEquals("test map.get(999_000)...", testValue_999_000, map.get(999_000, 1_000_000));
        System.out.println("ElapsedTime = " + elapsedTime);
    }

    private static String getObject() {
        return "Object_" + countObject++;
    }
}
