package com.getjavajob.training.algo08.mamaeva.lesson06;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.printTestResult;

public class AssociativeArrayTest {
    public static void main(String[] args) {
        AssociativeArray<String, String> aa = new AssociativeArray<>();
        aa.add(null, "value_null");
        assertEquals("test add(null, value)...", "value_null", aa.add(null, "value_null_2"));
        assertEquals("test add(null, value)...", "value_null_2", aa.add(null, "value_null_3"));
        aa.add("polygenelubricants", "Value_polygenelubricants");
        assertEquals("test add..", null, aa.add("GydZG_", "Value_GydZG_"));
        assertEquals("test add..", null, aa.add("DESIGNING WORKHOUSES", "Value_DSIGNING WORKHOUSES"));
        aaTest(aa);

        printTestResult();
    }

    private static void aaTest(AssociativeArray<String, String> aa) {
        for (int i = 0; i < 16; i++) {
            aa.add("key_" + i, "value_" + i);
        }
        for (int i = 8; i < 13; i++) {
            aa.remove("key_" + i);
        }
        for (int i = 0; i < 5; i++) {
            aa.add("key_" + i, "newValue_" + i);
        }
        aa.add(null, "value_null");
        String[] valuesExpected = {
                "newValue_0",
                "newValue_1",
                "newValue_2",
                "newValue_3",
                "newValue_4",
                "value_5",
                "value_6",
                "value_7",
                null,
                null,
                null,
                null,
                null,
                "value_13",
                "value_14",
                "value_15",
                null,
                null
        };
        for (int i = 0; i < 18; i++) {
            assertEquals("test add, remove, get...", valuesExpected[i], aa.get("key_" + i));
        }
    }
}

