package com.getjavajob.training.algo08.mamaeva.lesson01;

public class Task06 {
    /**
     * @param n
     * @return 2^n
     */
    public static int twoPowN(int n) {
        return 1 << n;
    }

    /**
     * @param n != m
     * @param m != n
     * @return 2^n + 2^m
     */
    public static int twoPowNPlusTwoPowM(int n, int m) {
        if (n == m) {
            return twoPowN(n + 1);
        }
        return 1 << n ^ 1 << m;
    }

    /**
     * @param a
     * @param n
     * @return a with reset n lower bits
     */
    public static int resetLowerBits(int a, int n) {
        int mask = -1 << n;
        return a & mask;
    }

    /**
     * @param a
     * @param n
     * @return set a's n-th bit with 1
     */
    public static int setBitOne(int a, int n) {
        int mask = 1 << n - 1;
        return a | mask;
    }

    /**
     * @param a
     * @param n
     * @return a with invert n-th bit
     */
    public static int invertBit(int a, int n) {
        int mask = 1 << n - 1;
        return a ^ mask;
    }

    /**
     * @param a
     * @param n
     * @return set a's n-th bit with 0
     */
    public static int setBitZero(int a, int n) {
        int mask = 1 << n - 1;
        return a & ~mask;
    }

    /**
     * @param a
     * @param n
     * @return n lower bits from a
     */
    public static int getLowerBits(int a, int n) {
        int mask = -1 << n;
        return a & ~mask;
    }

    /**
     * @param a
     * @param n
     * @return n-th bit from a
     */
    public static int getBit(int a, int n) {
        return (a >> n - 1) & 1;
    }

    /**
     * @param a - byte
     * @return bit representation of a using bit ops
     */
    public static String byteToBinaryString(byte a) {
        StringBuilder stringBuilder = new StringBuilder();
        if (a < 0) {
            stringBuilder.append(1);
        }
        for (int i = 7; i > 0; i--) {
            stringBuilder.append(getBit(a, i));
        }
        return stringBuilder.toString();
    }
}
