package com.getjavajob.training.algo08.mamaeva.lesson05;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.*;

public class QueueTest {
    private static Queue<Object> queue = new ArrayDeque<>();
    private static List<Object> list = new ArrayList<>();
    private static int countObjects;

    public static void main(String[] args) {
        addTest();
        offerTest();
        clear();
        elementExceptionTest();
        assertEquals("test queue.peek()...", null, queue.peek());
        assertEquals("test queue.poll()...", null, queue.poll());
        removeExceptionTest();
        addTest();
        assertEquals("test queue.element()...", list.get(0), queue.element());
        assertEquals("test queue.peek()...", list.get(0), queue.peek());
        assertEquals("test queue.peek()...", list.toArray(), queue.toArray());
        pollTest();
        removeTest();

        printTestResult();
    }

    private static void removeExceptionTest() {
        try {
            queue.remove();
            fail("test queue.remove() with empty queue...missed expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test queue.remove() with empty queue with exception...", NoSuchElementException.class, e.getClass());
        }
    }

    private static void removeTest() {
        assertEquals("test queue.remove()...", list.get(0), queue.remove());
        list.remove(0);
        assertEquals("test queue.remove()...", list.toArray(), queue.toArray());
    }

    private static void pollTest() {
        assertEquals("test queue.poll()...", list.get(0), queue.poll());
        list.remove(0);
        assertEquals("test queue.poll()...", list.toArray(), queue.toArray());
    }

    private static void elementExceptionTest() {
        try {
            queue.element();
            fail("test queue.element() with empty queue...missed expected NoSuchElementException");
        } catch (Exception e) {
            assertEquals("test queue.element() with empty queue with exception...", NoSuchElementException.class, e.getClass());
        }
    }

    private static void clear() {
        queue.clear();
        list.clear();
    }

    private static void offerTest() {
        Object o = getNewObject();
        queue.offer(o);
        list.add(o);
        assertEquals("test queue.offer(o)...", list.toArray(), queue.toArray());
    }

    private static void addTest() {
        for (int i = 0; i < 5; i++) {
            String o = getNewObject();
            queue.add(o);
            list.add(o);
        }
        assertEquals("test queue.add(Object)...", list.toArray(), queue.toArray());
    }

    private static String getNewObject() {
        return "Object" + countObjects++;
    }
}
