package com.getjavajob.training.algo08.mamaeva.lesson07.tree.binary;

import com.getjavajob.training.algo08.mamaeva.lesson07.tree.Node;

import static com.getjavajob.training.algo08.util.Assert.*;

public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        ArrayBinaryTree<String> abt = new ArrayBinaryTree<>();
        Node<String> root = abt.addRoot("root");
        Node<String> n1 = abt.addLeft(root, "n1");
        Node<String> n2 = abt.addRight(root, "n2");
        Node<String> n11 = abt.addLeft(n1, "n11");
        Node<String> n12 = abt.addRight(n1, "n12");
        Node<String> n121 = abt.addLeft(n12, "n121");
        Node<String> n1212 = abt.addRight(n121, "n1212");
        Node<String> n21 = abt.addLeft(n2, "n21");
        Node<String> n22 = abt.addRight(n2, "n22");
        Node<String> n212 = abt.addRight(n21, "n212");

        assertEquals("test addRoot(), root()...", root, abt.root());
        testRoot(abt, root);
        assertEquals("test addLeft(Node, e), left()...", n21, abt.left(n2));
        testAddLeft(abt, n1);
        assertEquals("test addRight(Node, e), right()...", n12, abt.right(n1));
        testAddRight(abt, n1);
        testAdd(abt, n212);
        assertEquals("test parent(Node)...", n1, abt.parent(n12));
        assertEquals("test parent(Node)...", n1, abt.parent(n11));
        assertEquals("test parent(Root)...", null, abt.parent(root));
        assertEquals("test set(Node, e)...", "n1212", abt.set(n1212, "n_1212"));
        assertEquals("test set(Node, e)...", "n_1212", abt.set(n1212, "n1212"));
        assertEquals("test size()...", 12, abt.size());
        testIterator(abt);
        assertEquals("test parent(Node)...", n12, abt.parent(n121));
        testRemove(abt, n121, n212);

        printTestResult();
    }

    private static void testRemove(ArrayBinaryTree<String> abt, Node<String> n121, Node<String> n212) {
        assertEquals("test remove(Node)...", "n212", abt.remove(n212));
        assertEquals("test remove(Node)...", "n121", abt.remove(n121));
        String expected = "root/n1/n2/n11/n12/n21/n22/";
        assertEquals("test iterator() after remove(Node)...", expected, printTree(abt));
    }

    private static void testIterator(ArrayBinaryTree<String> abt) {
        String expected = "root/n1/n2/n11/n12/n21/n22/n121/n212/n1212/n2121/n2122/";
        assertEquals("test iterator(), nodes()...", expected, printTree(abt));
    }

    private static void testAdd(ArrayBinaryTree<String> abt, Node<String> n212) {
        Node<String> n2121 = abt.add(n212, "n2121");
        Node<String> n2122 = abt.add(n212, "n2122");
        try {
            abt.add(n212, "n2121");
            fail("test add(Node, e) with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test add(Node, e) with exception...", "the node has left and right child already", e.getMessage());
        }
        try {
            abt.add(n212, "n2122");
            fail("test add(Node, e) with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test add(Node, e) with exception...", "the node has left and right child already", e.getMessage());
        }
    }

    private static void testAddRight(ArrayBinaryTree<String> abt, Node<String> n1) {
        try {
            abt.addRight(n1, "n1");
            fail("test addRight() with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test addRoot() with exception...", "the right child is already exist", e.getMessage());
        }
    }

    private static void testAddLeft(ArrayBinaryTree<String> abt, Node<String> n1) {
        try {
            abt.addLeft(n1, "n1");
            fail("test addLeft() with exception...expected IllegalArgumentException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            assertEquals("test addRoot() with exception...", "the left child is already exist", e.getMessage());
        }
    }

    private static void testRoot(ArrayBinaryTree<String> abt, Node<String> root) {
        try {
            abt.addRoot("newRoot");
            fail("test addRoot() with exception...expected IllegalStateException");
        } catch (AssertionError e) {
            System.out.println(e);
        } catch (IllegalStateException e) {
            assertEquals("test addRoot() with exception...", "the root is already exist", e.getMessage());
        }
    }

    private static String printTree(ArrayBinaryTree<String> abt) {
        StringBuilder actual = new StringBuilder();
        for (String aAbt : abt) {
            if (aAbt != null) {
                actual.append(aAbt).append("/");
            }
        }
        return actual.toString();
    }
}
