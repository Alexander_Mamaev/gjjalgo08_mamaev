package com.getjavajob.training.algo08.mamaeva.lesson06;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedHashSetTest {
    private static int countObjects;

    public static void main(String[] args) {
        Set<String> set = new LinkedHashSet<>();
        String[] expectedLinkedSetOrder = new String[]{"Object_0", "Object_1", "Object_2", "Object_3", "Object_4", "Object_5", "Object_6", "Object_7", "Object_8", "Object_9"};

        fillSet(set);
        assertEquals("test set.add()...", expectedLinkedSetOrder, set.toArray());
        assertEquals("test set.add() with duplicates...", false, set.add("Object_2"));

    }

    private static void fillSet(Set<String> set) {
        for (int i = 0; i < 10; i++) {
            set.add("Object_" + countObjects++);
        }
    }
}
