package com.getjavajob.training.algo08.mamaeva.lesson04;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.*;

public class DynamicArrayTest {
    private static List<Object> list = getFilledList(15);
    private static DynamicArray dynamicArray = getFilledDynamicArray(list);
    private static int countNewObjects;

    public static void main(String[] args) {
        testDefaultConstructor();
        testConstructor();
        testAdd();
        testAddWithIndex();
        testAddWithIndexException();
        testSet();
        testSetWithIndexException();
        testGet();
        testGetWithIndexException();
        testRemove();
        testRemoveWithIndexException();
        testRemoveObject();
        testSize();
        testIndexOf();
        testContains();
        testToArray();
        testIsEmpty();

        printTestResult();
    }

    private static void testIsEmpty() {
        try {
            dynamicArray.isEmpty();
            fail("test dLL.isEmpty()... missed expected UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("Test non implemented 'isEmpty()' method which called exception...", UnsupportedOperationException.class, e.getClass());
        }
    }

    private static void testToArray() {
        assertEquals("Test dLL.toArray()...", list.toArray(), dynamicArray.toArray());
    }

    private static void testContains() {
        Object o = dynamicArray.get(7);
        assertEquals("Test dLL.contains(Object)...", list.contains(o), dynamicArray.contains(o));
        assertEquals("Test dLL.contains(Object)...", list.contains("nonexistent"), dynamicArray.contains("nonexistent"));
        assertEquals("Test dLL.contains(Object)...", list.contains(null), dynamicArray.contains(null));
        testToArray();
    }

    private static void testIndexOf() {
        Object o = dynamicArray.get(7);
        assertEquals("Test dLL.indexOf(Object)...", list.indexOf(o), dynamicArray.indexOf(o));
        assertEquals("Test dLL.indexOf(Object)...", list.indexOf("nonexistent"), dynamicArray.indexOf("nonexistent"));
        assertEquals("Test dLL.indexOf(Object)...", list.indexOf(null), dynamicArray.indexOf(null));
        testToArray();
    }

    private static void testSize() {
        assertEquals("Test dLL.size()...", list.size(), dynamicArray.size());
    }

    private static void testRemoveObject() {
        Object o = dynamicArray.get(7);
        assertEquals("Test dLL.removeHead(Object)...", list.remove(o), dynamicArray.remove(o));
        assertEquals("Test dLL.removeHead(Object)...", list.remove("nonexistent"), dynamicArray.remove("nonexistent"));
        assertEquals("Test dLL.removeHead(Object)...", list.remove(null), dynamicArray.remove(null));
        testToArray();
    }

    private static void testRemoveWithIndexException() {
        try {
            dynamicArray.remove(dynamicArray.size() + 1);
            fail("testRemoveWithIndexException...missed expected Index Out Of Bound exception");
        } catch (Exception e) {
            assertEquals("Test 'removeHead' method with index exception...", IndexOutOfBoundsException.class, e.getClass());
        }
    }

    private static void testRemove() {
        assertEquals("Test dLL.removeHead(0)...", list.remove(0), dynamicArray.remove(0));
        assertEquals("Test dLL.removeHead(5)...", list.remove(5), dynamicArray.remove(5));
        assertEquals("Test dLL.removeHead(size-1)...", list.remove(list.size() - 1), dynamicArray.remove(dynamicArray.size() - 1));
        testToArray();
    }

    private static void testGetWithIndexException() {
        try {
            dynamicArray.get(dynamicArray.size() + 1);
            fail("testGetWithIndexException...missed expected Index Out Of Bound exception");
        } catch (Exception e) {
            assertEquals("Test 'get' method with index exception...", IndexOutOfBoundsException.class, e.getClass());
        }
    }

    private static void testGet() {
        assertEquals("Test 'get' method...", list.get(0), dynamicArray.get(0));
        assertEquals("Test 'get' method...", list.get(7), dynamicArray.get(7));
        assertEquals("Test 'get' method...", list.get(list.size() - 1), dynamicArray.get(dynamicArray.size() - 1));
        testToArray();
    }

    private static void testSetWithIndexException() {
        try {
            dynamicArray.set(dynamicArray.size() + 1, getNewObject());
            fail("testSetWithIndexException...missed expected Index Out Of Bound exception");
        } catch (Exception e) {
            assertEquals("Test 'set' method with index exception...", IndexOutOfBoundsException.class, e.getClass());
        }
    }

    private static void testSet() {
        Object newObject = getNewObject();
        assertEquals("Test dLL.set(0, newObject)...", list.set(0, newObject), dynamicArray.set(0, newObject));
        assertEquals("Test dLL.set(5, newObject)...", list.set(5, newObject), dynamicArray.set(5, newObject));
        assertEquals("Test dLL.set(size-1, newObject)...", list.set(list.size() - 1, newObject), dynamicArray.set(dynamicArray.size() - 1, newObject));
        assertEquals("Test dLL.set(3, null)...", list.set(3, null), dynamicArray.set(3, null));
        testToArray();
    }

    private static void testAddWithIndexException() {
        try {
            dynamicArray.add(dynamicArray.size() + 1, getNewObject());
            fail("testAddWithIndexException...missed expected Index Out Of Bound exception");
        } catch (Exception e) {
            assertEquals("Test 'add' method with index exception...", IndexOutOfBoundsException.class, e.getClass());
        }
    }

    private static void testAddWithIndex() {
        Object newObject = getNewObject();
        dynamicArray.add(0, null);
        list.add(0, null);
        dynamicArray.add(3, null);
        list.add(3, null);
        list.add(0, newObject);
        list.add(5, newObject);
        list.add(list.size(), newObject);
        dynamicArray.add(0, newObject);
        dynamicArray.add(5, newObject);
        dynamicArray.add(dynamicArray.size(), newObject);
        assertEquals("Test 'add(int index)' and size() methods...", list.toArray(), dynamicArray.toArray());
    }

    private static void testAdd() {
        Object newObject = getNewObject();
        assertEquals("Test dLL.add(null) ...", list.add(null), dynamicArray.add(null));
        assertEquals("Test dLL.add(null) ...", list.add(null), dynamicArray.add(null));
        assertEquals("Test dLL.add()...", list.add(newObject), dynamicArray.add(newObject));
        assertEquals("Test dLL.add()...", list.toArray(), dynamicArray.toArray());
    }

    private static void testConstructor() {
        List<Object> list = getFilledList(15);
        DynamicArray dynamicArray = getFilledDynamicArray(list);
        assertEquals("Test add method with dLL(int i) constructor...", list.toArray(), dynamicArray.toArray());
    }

    private static void testDefaultConstructor() {
        List<Object> list = getFilledList(7);
        DynamicArray dynamicArray = getFilledDynamicArray(list);
        assertEquals("Test add method and dLL default constructor...", list.toArray(), dynamicArray.toArray());
    }

    private static DynamicArray getFilledDynamicArray(List<Object> list) {
        DynamicArray dynamicArray;
        if (list.size() > 10) {
            dynamicArray = new DynamicArray(list.size());
        } else {
            dynamicArray = new DynamicArray();
        }
        for (Object o : list) {
            dynamicArray.add(o);
        }
        return dynamicArray;
    }

    private static List<Object> getFilledList(int size) {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add("Object" + i);
        }
        return list;
    }

    private static Object getNewObject() {
        return "newObject" + countNewObjects++;
    }
}