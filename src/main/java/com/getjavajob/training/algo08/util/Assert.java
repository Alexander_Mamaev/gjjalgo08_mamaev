package com.getjavajob.training.algo08.util;

import java.util.Arrays;
import java.util.Collection;

public class Assert {
    private static final String PASSED = "\u001B[32m PASSED \u001B[0m";
    private static final String FAILED = "\u001B[31m FAILED \u001B[0m";
    private static boolean testPassed = true;

    private static boolean isTestPassed() {
        return testPassed;
    }

    private static void setTestFailed() {
        testPassed = false;
    }

    public static void fail(String msg) {
        setTestFailed();
        throw new AssertionError("\u001B[31m" + msg + "\u001B[0m");
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + PASSED + ": expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + FAILED + "failed: expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + PASSED + ": expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + FAILED + ": expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.printf("%s: expected %c, actual %c%n", testName + PASSED, expected, actual);
        } else {
            System.out.printf("%s: expected %c, actual %c%n", testName + FAILED, expected, actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + PASSED + ": expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + FAILED + ": expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected == null && actual == null) {
            System.out.println(testName + PASSED + ": expected null, actual null");
        } else if (expected.equals(actual)) {
            System.out.println(testName + PASSED + ": expected " + expected + ", actual " + actual);
        } else {
            System.out.println(testName + FAILED + ": expected " + expected + ", actual " + actual);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, double[][] expected, double[][] actual) {
        if (expected.length != actual.length) {
            setTestFailed();
            return;
        } else {
            for (int j = 0; j < expected.length; j++) {
                if (expected[j].length != actual[j].length) {
                    setTestFailed();
                    return;
                }
            }
        }
        System.out.println(testName + System.lineSeparator() + "expected ... actual");
        for (int j = 0; j < expected.length; j++) {
            for (int i = 0; i < expected[j].length; i++) {
                if (expected[j][i] != actual[j][i]) {
                    setTestFailed();
                }
                printArray(expected[j]);
                System.out.print("... ");
                printArray(actual[j]);
                System.out.println();
            }
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        assertEquals(testName, expected, actual, true);
    }

    public static void assertEquals(String testName, int[] expected, int[] actual, boolean printResult) {
        boolean testResult = true;
        if (expected.length != actual.length) {
            setTestFailed();
            System.out.println("The arrays have not equal length for comparison!");
            return;
        } else {
            for (int i = 0; i < expected.length; i++) {
                if (expected[i] != actual[i]) {
                    testResult = false;
                    setTestFailed();
                }
            }
        }
        if (testResult) {
            System.out.println(testName + PASSED);
        } else {
            System.out.println(testName + FAILED);
        }
        if (printResult) {
            System.out.print("expected ... ");
            printArray(expected);
            System.out.println();
            System.out.print("actual ..... ");
            printArray(actual);
            System.out.println();
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + PASSED);
        } else {
            System.out.println(testName + FAILED);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, Object expected, Object actual) {
        if ((expected == null & actual == null) || (expected != null & actual != null) && expected.equals(actual)) {
            System.out.println(testName + PASSED);
        } else {
            System.out.println(testName + FAILED);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, Object[] expected, Object[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + PASSED);
        } else {
            System.out.println(testName + FAILED);
            setTestFailed();
        }
    }

    public static void assertEquals(String testName, Collection expected, Collection actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + PASSED);
        } else {
            System.out.println(testName + FAILED);
            setTestFailed();
        }
    }

    private static void printArray(int[] array) {
        for (int arrayElement : array) {
            System.out.print(arrayElement + " ");
        }
    }

    private static void printArray(double[] array) {
        for (double arrayElement : array) {
            System.out.print(arrayElement + " ");
        }
    }

    public static void printTestResult() {
        System.out.println();
        System.out.println("TEST" + (isTestPassed() ? PASSED : FAILED));
    }
}
